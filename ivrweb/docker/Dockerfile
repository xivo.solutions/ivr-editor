ARG PHP_VERSION

FROM yiisoftware/yii2-php:${PHP_VERSION}-fpm

RUN apt update \
	&& apt -y install procps zip sox mpg123 \
	&& rm -rf /var/lib/apt/lists/*

ENV COMPOSER_HOME=/.composer

COPY ivragi/docker/files/.composer /.composer

RUN mkdir -p /var/lib/xivo/ivrsounds

RUN composer create-project -qn --ansi --prefer-dist yiisoft/yii2-app-basic:2.0.45 /app && \
    composer require yiisoft/yii2-bootstrap yiisoft/yii2-jui kartik-v/yii2-widget-typeahead kartik-v/yii2-widget-sidenav

RUN rm -r -- \
        controllers/* \
        models/* \
        views/* \
        web/index-test.php

COPY app/assets/ assets/
COPY app/assets-src/ assets-src/
COPY app/components/ components/
COPY app/config/ config/
COPY app/controllers/ controllers/
COPY app/mail/ mail/
COPY app/models/ models/
COPY app/views/ views/
COPY app/web/ web/
COPY app/widgets/ widgets/
COPY app/messages/ messages/
COPY ivrweb/docker/files/docker-php-entrypoint /usr/local/bin/

ARG TARGET_VERSION
LABEL version=${TARGET_VERSION}
