echo "Pulling Yii's image and building our test environment, it may take some time if the image was never pulled before"
docker build --quiet --build-arg PHP_VERSION=7.4 -t ivr-specs -f SpecsDockerfile .
echo "Starting the test environment"
CONTAINER_ID=$(docker run -d --env DB_USER=foobar ivr-specs:latest)
docker exec -i $CONTAINER_ID ./vendor/bin/codecept run
echo "Removing and cleaning up leftovers"
docker rm -f $CONTAINER_ID