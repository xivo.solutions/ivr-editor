# IVR-editor

## Unit tests
You need docker installed locally (tested with docker 20.10.7, build f0df350).
Start `run_tests.sh` in your CLI.
A docker image will be created with your changes and a container will be mounted on the fly on your host to run the specs with the correct environment configuration. The specs will run with Yii's instance of Codeception.
The container will be deleted when the specs are done and the specs results will stay visible in your CLI.

## How to create new flowchart node type

Use other node files as an example.

To create new node type "foo":

### Create Form Model

In directory models create file FormNodeprmFoo.php containing class FormNodeprmFoo.
File and class name must be derived from node type name.

Define all node parameter as public variables of the class 

Redefine class methods
- exprAttributes(): returns array of names of parameters which accept expression as a parameter value
- runtimeRules(): returns array with Yii2 validators of parameters
- attributeLabels(): returns associatve array 'param_name' => 'param_label'
- automaticName(): returns string - node name derived from node parameters
  and used when node.name is empty

### Create parameters form view

In views/node directory create file _form_nodeprm_foo.php 
File name must be derived from node type name.

For dropbox/listbox fields, which can also accept expression, use
MyActiveField class. See e.g. _form_nodeprm_menu.php


### Create subclass of DialplanNode

In components directory create file fooNode.php containing class fooNode.

Rewrite class methods if necessary
- optList() - returns array of node exits options
- run(($agi, &$dp_status, &$user_variables) - runs node during call
- getTitle() - returns node type label used in editor in menu and on node diagrams

**function run()**
$agi: agi object allowing communication with asterisk (see components/AGI.php) \
$dp_status: associative array allowing pass global dialplan status to the nodes (only used for state of recording now) \
$user_variables: varibles defined in flowchart (NOT asterisk channel variables)

Function must return one of values returned by optList method or value '_error' when runtime error occurs.

### Add node type to DialplanNode class

In file components/DialplanNode.php

add line \
use app\componnets\fooNode;

add "foo" to the $_node_type array


## Usage

### Nodes

#### start

Starting node of the flowchart. Is created automatically and cannot be
deleted. Only one node of type start can be present in flowchart.

**Parameters**
*Node has no parameters*

**Exits**
- next


#### onerror

This node starts flowchart part which is used after runtime error.
The node has no input therefore.
Only one node of type onerror can be present in flowchart.

**Parameters**
*Node has no parameters*

**Exits**
- next


#### assign
Assigns value to variable

**Parameters**
- Variable: variable name
- Value: value to assign, can be expression

**Ouputs**
- next


#### cel
Runs CELGenUserEvent dialplan application

**Parameters**
- Name: event name
- Message: Extra text to be included with the event

**Exits**
- next

#### condition
Evaluates condition and continues in flowchart according the result

**Parameters**
- Condition: condition to evaluate

**Exits**
- 1: used when condition is evaluated as true
- 0: used when condition is evaluated as false

#### goto
Sends call to the priority 1 in extension@context and exits flowchart AGI script

**Parameters**
- Context: context call to be sent to
- Extension: extension call to be sent to

**Exits**
*Node has no exits*

#### group
Sends call to the priority 1 in extension@context of given group and exits flowchart AGI script

**Parameters**
- Group name: name of group to be called

**Exits**
*Node has no exits*


#### hangup
Hangups the call and exits flowchart AGI script

**Parameters**
*Node has no parameters*

**Exits**
*Node has no exits*


#### http
Sends HTTP(S) request
In GET request variables are used in URL query string
In POST request variables are send as application/x-www-form-urlencoded data

**Parameters**
- URL: URL of the request
- Method: Request method
- Variables: space separated list of variables used in request 

**Exits**
- next

#### menu
* Plays file set in parameter 'Menu voice prompt'
* Waits for DTMF digit, waiting is limited by 'Timeout' parameter
* If DTMF digit is received and is connected exit opt, corresponding node exit is used.
* If DTMF digit received is not connected exit opt, file defined in parameter 'Invalid voiceprompt' is played and node is started again, unless number of tries defined in parameter 'Repeat' is reached.
* If no DTMF digit is received till timeout, file defined in parameter 'No choice voiceprompt' is played and and node is started again, unless number of tries defined in parameter 'Repeat' is reached.
* Node uses 'f' exit

**Parameters**
- Repeat: Number of attempt to receive DTMF digit
- Timeout: Time in seconds fir which node waits for a DTMF digit
- Menu voiceprompt: File played at node start
- No choice voiceprompt: File played when no digit has been sent
- Invalid voiceprompt: File played when unexpected DTMF digit has been sent

**Exits**
- 0-9,A-D,*,#: used when corresponding DTMF digit has been sent and exit is connected to the other node
- f: used when valid choice has not been sent


#### playback
Play selected file

**Parameters**
- Voice prompt file: file to play

**Exits**
- next


#### queue 
Exits flowchart AGI script and send dialplan to selected queue

**Parameters**
- Name: queue name

**Exits**
*node has no exit*


#### read 
Reads sequens of DTMF digits terminated by '#'

**Parameters**
- Voice prompt file: File played at node start
- Invalid voice prompt: File played when invalid input has been sent
- Repeat: Max. number of attempts
- Min digits: Minimal number of digits in input
- Max digits: Maximal number of digits in input
- Numeric: If checked numeric value is expected
- Min value: Minimal value of numeric input
- Max value: Maximal value of numeric input
- Beep: Beep before input
- Variable: Store input  in this variable

**Exits**
- next


#### saynumber
Says number using Saynumber dialplan application

**Parameters**
- Number: Number to be said

**Exits**
- next

#### user
Sends call to the priority 1 in extension@context defined for selected user
in tables userfeatures and linefeatures.
Parameter form allows to select user by name or extention

**Parameters**
- id: hidden parameter populated by searching in User or Number fields

**Exits**
*Node has no exits*


#### voicemail
Sends call to selected voicemail
Parameter form allows to select voicemail by name or mailbox number

**Parameters**
- id: hidden parameter populated by searching in Fullame or Mailbox fields

**Exits**
*Node has no exits*




####  wait
Wait for defined number of seconds. Dialplan application WAIT is used.

**Parameters**
- Seconds: Number of seconds to wait

**Exits**
- next


### Varibles

Flowchart variables are NOT asterisk channel variables. \
They can only be set in flowchart nodes (e.g. assign or menu nodes) and they \
live only during flowchart run.

Variables names always start with $ (dollar) \
Variables can be used in expressions


### Expressions in parameters

Some fields in node parameter form accept expressions.
Expression always starts with '=' (equal sign) and can contain numbers,
strings, variables, parentheses and php operators . + - * / ! && || 

So to a field accepting integer number you can write \
1		directly expresed number \
= 1 + 1 	expression \
= $a + 1	expression with variable \

but NOT \
1 + 1


Similary with strings, correct values are \
aaa \
= "aaa" \
= "aa" . 'a' \
= $a . "aaa"


Constant values in node parameter forms are validated when form is
submited. Same for syntax of expressions.
Additionaly result of expression is validated during runtime and failed
validation causes flowchart node exits with '_error' result.
