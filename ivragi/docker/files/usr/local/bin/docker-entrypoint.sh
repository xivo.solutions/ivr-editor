#!/bin/sh

if [ -z "$DB_USER" ]; then
	echo "DB_USER not set" >&2
	exit 1
fi

sed -i -e "s#%DB_USER%#${DB_USER}#" -e "s#%DB_PASSWORD%#${DB_PASSWORD}#" /app/config/db.php

exec /usr/sbin/xinetd -dontfork

