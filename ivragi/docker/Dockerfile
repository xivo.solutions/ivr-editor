ARG PHP_VERSION

FROM php:${PHP_VERSION}-cli

RUN apt update \
	&& apt install -y procps zip xinetd \
	&& rm -rf /var/lib/apt/lists/*

# Install system packages for PHP extensions recommended for Yii 2.0 Framework
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN install-php-extensions \
        soap \
        zip \
        bcmath \
        exif \
        gd \
        intl \
        opcache \
        pdo_pgsql \
        imagick \
        xdebug-3.1.6 \
	pcntl

# Disable xdebug by default (see PHP_ENABLE_XDEBUG)
RUN rm /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

# Environment settings
ENV COMPOSER_ALLOW_SUPERUSER=1 \
    PHP_USER_ID=33 \
    PHP_ENABLE_XDEBUG=0 \
    PATH=/app:/app/vendor/bin:/root/.composer/vendor/bin:$PATH \
    TERM=linux \
    COMPOSER_HOME=/.composer

# Add configuration files
COPY ivragi/docker/files/ /

RUN chmod 755 /usr/local/bin/composer

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- \
        --filename=composer.phar \
        --install-dir=/usr/local/bin && \
    composer clear-cache


# Install Yii framework bash autocompletion
RUN mkdir /etc/bash_completion.d && \
    curl -L https://raw.githubusercontent.com/yiisoft/yii2/master/contrib/completion/bash/yii \
         -o /etc/bash_completion.d/yii

# Application environment
WORKDIR /app


RUN echo "fastagi 4573/tcp" >> /etc/services

RUN mkdir -p /var/lib/xivo/ivrsounds

RUN composer create-project -qn --ansi --prefer-dist yiisoft/yii2-app-basic:2.0.45 /app 

RUN rm -r -- \
        controllers/* \
        models/* \
        views \
        web \
	widgets

COPY app/commands/ commands/
COPY app/components/ components/
COPY app/config/ config/
COPY app/mail/ mail/
COPY app/models/ models/



ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

EXPOSE 4573/tcp

ARG TARGET_VERSION
LABEL version=${TARGET_VERSION}
