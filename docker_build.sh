#!/bin/sh

set -e

PHP_VERSION=7.4

if [ -z $TARGET_VERSION ]; then
     echo "TARGET_VERSION is not available"
     exit -1
fi

if [ -z $PHP_VERSION ]; then
     echo "PHP_VERSION is not available"
     exit -1
fi

docker build --build-arg TARGET_VERSION=$TARGET_VERSION --build-arg PHP_VERSION=$PHP_VERSION -t xivoxc/xivo-ivrweb:$TARGET_VERSION -f docker/Dockerfile ..
