<?php

namespace app\controllers;

use Yii;
use app\models\Voicemail;

use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\db\Expression;

/**
 * FlowchartController implements the CRUD actions for Flowchart model.
 */
class VoicemailController extends Controller
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
            ]
        ];
    }

    public function actionNamesearch($q)
    {
        $this->layout = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (preg_match("/['%_]/", $q)) {
            return [];
        }

        $list = Voicemail::find()->select(['uniqueid', 'mailbox', 'fullname'])
            ->where(['like', new Expression('lower(unaccent(fullname))'), new Expression('lower(unaccent(:q))', ['q'=>"%{$q}%"])])
            ->asArray()
            ->all();

        return $list;
    }

    public function actionMailboxsearch($q)
    {
        $this->layout = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (!preg_match("/^[[:digit:]]+\$/", $q)) {
            return [];
        }

        $list = Voicemail::find()->select(['uniqueid', 'mailbox', 'fullname'])
            ->where(['like', new Expression('lower(mailbox)'), new Expression('lower(:q)', ['q'=>"%{$q}%"])])
            ->asArray()
            ->all();

        return $list;
    }
}
