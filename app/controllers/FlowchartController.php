<?php

namespace app\controllers;

use Yii;
use app\models\Flowchart;
use app\models\Node;
use app\models\Nodepos;
use app\models\FormNodeprm;
use app\models\PrmForm;

use app\components\DialplanNode;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;

/**
 * FlowchartController implements the CRUD actions for Flowchart model.
 */
class FlowchartController extends Controller
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all Flowchart models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Flowchart::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Creates a new Flowchart model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Flowchart();
        
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            if ($model->save()) {
                if ($this->makeInitNode($model->id)) {
                    $trans->commit();
                    return $this->redirect(['graph', 'id' => $model->id]);
                }
            }
            $trans->rollback();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Flowchart model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Duplicates an existing Flowchart model.
     * If duplication is successful, the browser will be redirected to the 'view' page.
     * @param integer $oldid
     * @paran string $newname
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDuplicate($id)
    {
        $orig = $this->findModelFull($id);
        $copy = new Flowchart();
        $copy->attributes = $orig->attributes;
        $copy->name = '';
        
        if ($copy->load(Yii::$app->request->post())) {
            $ok = true;
            $trans = Yii::$app->db->beginTransaction();

            if ($copy->save()) {
                foreach ($orig->nodes as $n) {
                    unset($n->id);
                    $n->id_flowchart = $copy->id;
                    $n->isNewRecord = true;
                    
                    if (!$n->save()) {
                        $ok = false;
                        break;
                    }
                    
                    $pos = $n->nodepos;
                    $pos->id_node = $n->id;
                    $pos->isNewRecord = true;
                    if (!$pos->save()) {
                        $ok = false;
                        break;
                    }
                    
                    foreach($n->nodeprms as $prm) {
                        unset($prm->id);
                        $prm->id_node = $n->id;
                        $prm->isNewRecord = true;
                        
                        if (!$prm->save()) {
                            $ok = false;
                            break 2;
                        }
                    }
                }                

                if ($ok) {
                    foreach($orig->nodecons as $c) {
                        unset($c->id);
                        $c->id_flowchart = $copy->id;
                        $c->isNewrecord = true;
                        
                        if (!$c->save()) {
                            $ok = false;
                            break;
                        }
                    }
                }
                
            } else {
                $ok = false;
            }       
                                            
            if ($ok) {
                $trans->commit();
                return $this->redirect(['index', 'id' => $copy->id]);
            } else {
                $trans->rollback();
            }
        }
            
        return $this->render('duplicate', [
            'orig' => $orig,
            'copy' => $copy
        ]); 

    }


    /**
     * Deletes an existing Flowchart model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->used()) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'NOT deleted. Flowchart is bind to Incoming call'));
        } else {
            $model->delete();
        }

        return $this->redirect(['index']);
    }


    public function actionGraph($id) {

        $model=$this->findModel($id);

        $list = DialplanNode::enabledNodes();
        $types = [];
        foreach ($list as $t) {
            $c = DialplanNode::typeToClassName($t);
            $types[$t] = array();
            $types[$t]['con'] = $c::optList();
            $types[$t]['title'] = $c::getLabel();
        }

        uasort($types, function($a, $b) {return strcmp($a['title'], $b['title']);});

        return $this->render('graph', [
            'model' => $model,
            'types' => $types
        ]);
    }

        /**
         * Ajax load for graph viewer/editor
         */

    public function actionLoad($id) {
        $this->layout=false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = $this->findModelForGraph($id);
        $nodes = [];
        foreach ($model->nodes as $n) {
            $narr = $n->toArray([], ['nodepos']);
            $narr['name'] = Html::encode($n->realName());
            $nodes[] = $narr;
        } 
            
        return [ 'nodes' => $nodes, 'nodecons' => $model->toArray([], ['nodecons'])['nodecons'] ];
    }



    /**
     * Finds the Flowchart model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Flowchart the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        $model = Flowchart::findOne($id);
        
        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }


    public function findModelFull($id) {
        $model = Flowchart::find()->where(['id' => $id])->
            with('nodes','nodes.nodeprms','nodecons','nodes.nodepos')->one();
        if ($model === null)
            throw new NotFoundHttpException(404,'The requested flowchart does not exist.');
        return $model;
    }


    public function findModelForGraph($id) {
        $model = Flowchart::find()->where(['id' => $id])->
            with('nodes','nodecons','nodes.nodepos')->one();
        if ($model === null)
            throw new NotFoundHttpException(404,'The requested flowchart does not exist.');
        return $model;
    }



    private function makeInitNode($id_flowchart) {
        /* DB transaction on calling function expected */
                
        $node = new Node();
        $node->id_flowchart = $id_flowchart;
        $node->node_number = 0;
        $node->node_type = 'start';
        $node->name = 'Start';

        if (!$node->save())
                return false;


        $pos = new Nodepos;
        $pos->id_node = $node->id;
        $pos->x = 250;
        $pos->y = 10;
        
        return $pos->save();
    }
}
