<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;

use app\models\Nodecon;
use app\models\Flowchart;

class NodeconController extends \yii\web\Controller
{


    public $layout = false;
    
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

        ];
    }


    public function actionCreate() {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                
		$model=new Nodecon;
		if ($model->load(Yii::$app->request->post())) {
                    $fch = Flowchart::findOne($model->id_flowchart);
                    if ($fch && $model->save())
                        return true;
		}
		
		return -1;
	}


	public function actionMove() {
	        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                	
	        $old = new Nodecon;
	        $new = new Nodecon;
	        
	        $data = Yii::$app->request->post();
	        
	        if ($new->load($data, 'New') && $old->load($data, 'Old')) {
        	        
        	        if ($old->id_flowchart !== $new->id_flowchart) {
                            return false;
                        }
                        
                        $fch = Flowchart::findOne($old->id_flowchart);
                        if ($fch === null) {
                            return false;
                        }
                                
                        
                        $transaction = Yii::$app->db->beginTransaction();
                        $md = $this->loadModel($old->id_flowchart, $old->src, $old->src_opt);
                        if ($md && $md->delete() && $new->save()) {
                                $transaction->commit();
                                return true;
                        }
                        
                        $transaction->rollback();
                }
                
                return false;
	}


	public function actionDelete() {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	        $this->layout = false;
		
		$model=new Nodecon;
		if ($model->load(Yii::$app->request->post())) {
                        $fch = Flowchart::findOne($model->id_flowchart);
			if (!$fch)
			    return false;
				
			
			$dm = $this->loadModel($model->id_flowchart, $model->src, $model->src_opt);
			if ($dm && $dm->delete())
                            return true;
		}
		
		return false;
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Nodecon the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id_flowchart, $src, $src_opt) {
	        $crit = array(
			'id_flowchart' => $id_flowchart,
			'src' => $src,
			'src_opt' => $src_opt
		);

                $model = Nodecon::find()->where($crit)->one();
		return $model;
	}

}
