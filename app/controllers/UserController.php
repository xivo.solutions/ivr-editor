<?php

namespace app\controllers;

use Yii;
use app\models\Userfeatures;
use app\models\Linefeatures;

use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\db\Expression;

/**
 * FlowchartController implements the CRUD actions for Flowchart model.
 */
class UserController extends Controller
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
            ]
        ];
    }

    public function actionUsersearch($q)
    {
        $this->layout = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (preg_match("/[%_]/", $q)) {
            return [];
        }

        $qq = Userfeatures::find()
        ->where([
            'or',
            ['like', new Expression('lower(unaccent(lastname))'), new Expression("lower(unaccent(:q))", ['q' => "%{$q}%"])],
            ['like', new Expression('lower(unaccent(firstname))'), new Expression("lower(unaccent(:q))", ['q' => "%{$q}%"])]
        ]);

//        print_r($qq->createCommand()->rawsql); exit;


        $list = Userfeatures::find()
        ->where([
            'or',
            ['like', new Expression('lower(unaccent(lastname))'), new Expression("lower(unaccent(:q))", ['q' => "%{$q}%"])],
            ['like', new Expression('lower(unaccent(firstname))'), new Expression("lower(unaccent(:q))", ['q' => "%{$q}%"])]
        ])
        ->with('lines')
        ->all();

        $out = [];
        foreach($list as $item) {
            if (!empty($item->lines)) {
                $out[] = [ 'name' => $item->firstname . ' ' . $item->lastname, 'id' => $item->id, 'number' => $item->lines[0]->number ];
            }
        }
        return $out;
    }

    public function actionNumbersearch($q)
    {
        $this->layout = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (!preg_match("/^[[:digit:]]+\$/", $q)) {
            return [];
        }

        $list = Linefeatures::find()->with('users')->where(['like', 'number', $q])->all();

        $out = [];
        foreach($list as $item) {
            if (!empty($item->users)) {
                $out[] = [ 'name' => $item->users[0]->firstname . ' ' . $item->users[0]->lastname, 'id' => $item->users[0]->id, 'number' => $item->number ];
            }
        }
        return $out;
    }
}
