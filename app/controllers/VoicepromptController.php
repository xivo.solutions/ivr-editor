<?php

namespace app\controllers;

use Yii;
use app\models\Voiceprompt;
use app\components\VoicePromptFile;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Sort;

/**
 * VoicepromptController implements the CRUD actions for Voiceprompt model.
 */
class VoicepromptController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Voiceprompt models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Voiceprompt::find(),
            'sort' => new Sort(['attributes' => ['name']]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreatevoiceprompt() {	
        $model = new Voiceprompt();
        $model->scenario = Voiceprompt::SC_UPLOAD;

        $ajax = Yii::$app->request->isAjax;
        if ($ajax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->validate()) {
                $model->source = Voiceprompt::SRC_UPLOAD;
                $transaction = Yii::$app->db->beginTransaction();
                $vpf = new VoicePromptFile(VoicePromptFile::TYPE_VP, $model->name);
                if ($vpf->exists()) {
                    $model->addError('name', Yii::t('app', 'Name already exists'));
                } else {
                    if ($model->save()) {
                        $extension = pathinfo($model->file->name, PATHINFO_EXTENSION);
                        $filename = $model->file->tempName . "." . $extension;
                        move_uploaded_file($model->file->tempName, $filename);
                        if (($ret = $vpf->save($filename)) == VoicePromptFile::ERR_OK) {
                            $transaction->commit();
                            if ($ajax) {
                                return true;
                            } else {
                                return $this->redirect(['index']);
                            }
                        } else {
                            $model->addError('file', VoicePromptFile::errMsg($ret));
                        }
                    }
                } 
                $transaction->rollback();
            }
        } else {
            if ($ajax)
                return "Invalid request data";
        }
        if ($ajax) {
            $err = '';
            $errors = $model->getFirstErrors();
            foreach ($errors as $e) {
                $err = $e;
                break;
            }
             
            if ($err == '') {
                $err = "Unknown error";
            }
            return $err;
        } else { 
            return $this->render('create', ['model' => $model]);
        }
    }

    public function actionChangefile($id) {
        $model = Voiceprompt::findOne($id);
        $model->scenario = Voiceprompt::SC_UPLOAD;

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->validate()) {
                $transaction = Yii::$app->db->beginTransaction();
                if ($model->save()) {
                    $uploadedFile = new VoicePromptFile(VoicePromptFile::TYPE_VP, $model->name);
                    $extension = pathinfo($model->file->tempName, PATHINFO_EXTENSION);
                    $filename = $model->file->tempName . "." . $extension;
                    move_uploaded_file($model->file->tempName, $filename);
                    if (($ret = $uploadedFile->save($filename)) == VoicePromptFile::ERR_OK) {
                        $transaction->commit();
                        return $this->redirect(['index']);
                    } else {
                        $model->addError('file', VoicePromptFile::errMsg($ret));
                    }
                }
                $transaction->rollback();
            }
        }
        return $this->render('upload', ['model' => $model]);
    }

    public function actionCreatetts() {
        $model = new Voiceprompt;
        $model->scenario = Voiceprompt::SC_TTS;

        if ($model->load(Yii::$app->request->post())) {
            $model->source = Voiceprompt::SRC_TTS;
                    
            if ($model->validate()) {
                $vp = new VoicePromptFile(VoicePromptFile::TYPE_VP, $model->name);
                        
                if ($vp->exists()) {
                    $model->addError('name', Yii::t('app', 'Name already exists'));
                } else { 
                    $trans = Yii::$app->db->beginTransaction();
                    $model->save();
                    if ($vp->mkFromText($model->msg, $model->lang, $model->speed)) {
                        $trans->commit();
                        return $this->redirect(['index']);
                    } else {
                        $model->addError('name', Yii::t('app', 'TTS failed'));
                    }
                    $trans->rollback();
                }
                    
            }
        }
        return $this->render('tts', ['model' => $model]);
    }
		

    public function actionUpdate($id) {
        $model = Voiceprompt::findOne($id);
        $oldname = $model->name;
        
        if ($model->load(Yii::$app->request->post())) {	
            // msg, lang, speed is unsafe on update scenario
            // to protect source of tts  VP
            if ($model->source != "tts") {
                $model->msg = $_POST['Voiceprompt']['msg'];
                // $model->lang = $_POST['Voiceprompt']['lang'];
                $model->speed = 1.0;
            }
            
            $trans = Yii::$app->db->beginTransaction();
            if ($model->save()) {
                $ok = 1;
                if ($model->name != $oldname) {
                    $vpf = new VoicePromptFile(VoicePromptFile::TYPE_VP, $oldname);
                    if (!$vpf->rename($model->name)) {
                        $model->addError('name', Yii::t("app", "File rename failed"));
                        $ok = 0;
                    }
                }
                    
                if ($ok) {
                    $trans->commit();
                    return $this->redirect(['index']);
                }
            }	
            $trans->rollback();
        }
    
	return $this->render('update', ['model' => $model]);		
    }

    public function actionUpdatetts($id) {
        $model = $this->findModel($id);
        $oldname = $model->name;
        
        if ($model->load(Yii::$app->request->post())) {
            $oldname = $model->name;
		
            $model->scenario = Voiceprompt::SC_TTS;
            $model->source = Voiceprompt::SRC_TTS;
		
            $trans = Yii::$app->db->beginTransaction();
            if ($model->save()) {
                $vpf = new VoicePromptFile(VoicePromptFile::TYPE_VP, $model->name);
                if ($vpf->mkFromText($model->msg, $model->lang, $model->speed)) {
                    if ($model->name != $oldname) {
                        $vpold = new VoicePromptFile(VoicePromptFile::TYPE_VP, $model->name, $oldname);
                        $vpold->delete();
                    }
			
                    $trans->commit();
                    return $this->redirect(['index']);
                }
            }
				
				
            $trans->rollback();
        }
        return $this->render('tts', array('model' => $model));
    }


    public function actionDelete($id) {
        $vp = $this->findModel($id);
        
        $trans = Yii::$app->db->beginTransaction();
        if ($vp->delete()) {	
            $vpf = new VoicePromptFile(VoicePromptFile::TYPE_VP, $vp->name);
            if ($vpf->delete()) {
                $trans->commit();
            } else { 
                $trans->rollback();
            }
        }

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax'])) {
            $this->redirect(['index']);
	}
    }


    public function actionMassupload() {
        $data = Yii::$app->request->post();

        if ($data) {
            print_r($data);
            exit;
        }

        return $this->render('massupload');
    }

    public function actionPlay($id) {
        $vp = Voiceprompt::findOne($id);
        if (!$vp)
            return;

        $rec = new VoicePromptFile(VoicePromptFile::TYPE_VP, $vp->name);
		
        if (!$rec->exists()) {
            return;
        }
        
        $sound_file = $rec->getPath();

        header('Content-type: audio/wav');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header("Content-Length: " . filesize($sound_file));

        @ob_clean();
        flush();
        readfile($sound_file);
    }	


    /**
     * Finds the Voiceprompt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Voiceprompt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if ($model = Voiceprompt::findOne($id)) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
