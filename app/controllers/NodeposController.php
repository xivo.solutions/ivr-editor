<?php

namespace app\controllers;

use Yii;
use app\models\Nodepos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * NodeposController implements the CRUD actions for Nodepos model.
 */
class NodeposController extends Controller
{

    public $layout = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionUpdate($id) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $model = Nodepos::findOne($id);
        return $model && $model->load(Yii::$app->request->post()) && $model->save();
    }


}
