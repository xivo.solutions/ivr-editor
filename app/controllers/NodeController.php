<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Html;

use app\models\Node;
use app\models\Flowchart;
use app\models\Nodepos;
use app\models\Nodeprm;
use app\models\FormNodeprm;
use app\models\PrmForm;
use app\components\DialplanNode;

class NodeController extends Controller
{
    
    public $layout = false;
    
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionCreate() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    
        $node = new Node;
        $pos = new Nodepos;
        
        if (!$node->load(Yii::$app->request->post()))
            return false;

        if (!in_array($node->node_type, DialplanNode::enabledNodes()))
            return false;

        if (!$pos->load(Yii::$app->request->post()))
            return false;

        $fch = Flowchart::findOne($node->id_flowchart);
        if ($fch === null)
            return false;


        if ($node->node_type == Node::NODE_TYPE_START && $node->number != 0)
                return false;
            
        if (in_array($node->node_type, Node::uniqueNodeTypes())) {
            if (Node::find()->where(['id_flowchart' => $node->id_flowchart, 'node_type' => $node->node_type])->exists())
                return false;
        }

        $node->name = '';
        $transaction = Yii::$app->db->beginTransaction();
        if (!$node->save()) {
            $transaction->rollback();
            return false;
        }
   
        $pos->id_node = $node->id;
        if (!$pos->save()) {
            $transaction->rollback();
            return false;
        }
          
        $transaction->commit();
        return [ 'id' => $node->id, 'name' => Html::encode($node->realName()) ];
    }


    public function actionUpdate($id) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = Node::findOne($id);
        if (!$model)
            return [ 'success' => false ];
  
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return [ 'success' => true, 'name' => Html::encode($model->realName()) ];
            } else { 
                $new = false;
            }
        } else {
            $new = true;
        }
        
        return [ 
            'success' => $new,
            'new' => $new,
            'html' => $this->render('update', ['model' => $model])
        ];
    }


    public function actionParameters($id) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = Node::findOne($id);
        if (!$model)
            return ['success' => false];

        $view = "_" . "form_nodeprm_" . $model->node_type;
        $fclass = FormNodeprm::formClass($model->node_type);
        $form = new $fclass;

        $newname = null;
        $oldform = PrmForm::loadForm($fclass, $model->id);
        if ($form->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            if ($form->save($model->id)) {
                $ok = true;
                $newname = null;
                if ($oldform && $oldform->automaticName() != $form->automaticName()) {
                    $model->name = '';
                    $ok = $model->save();
                    $newname = $form->automaticName();
                }
                if ($ok) {
                    $transaction->commit();
                    return ['success' => true, 'new' => true, 'name' => $newname ? Html::encode($newname) : null];
                }
            }
            $transaction->rollback();
            $extra_data = $form->viewExtraData();
            return ['success' => false, 'html' => $this->renderAjax($view, array('model' => $form, 'data' => $extra_data))];
        } else {
            $form = PrmForm::loadForm($fclass, $model->id);
        }
        $extra_data = $form->viewExtraData();
        return ['success' => true, 'new' => true, 'html' => $this->renderAjax($view, array('model' => $form, 'extra_data' => $extra_data))];
    }


    public function actionDelete($id) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = Node::findOne($id);
        if (!$model)
            return 0;

        return $model->node_number > 0 && $model->delete() !== false;
    }
}
