<?php

namespace app\controllers;

use Yii;
use app\models\LoginForm;

class SiteController extends \yii\web\Controller
{
    public function actionIndex() {
        return $this->redirect(['/flowchart/index']);
    }

/*
    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Yii::$app->user->authTimeout = 3600 * 24;
            return $this->redirect(['/site/index']);
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
*/
}
