<?php

namespace app\assets;

use yii\web\AssetBundle;

class PlayfileAsset extends AssetBundle
{
    public $sourcePath = '@app/assets-src/playfile';
    public $js = [
        'js/playfile.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
