<?php

namespace app\assets;

use yii\web\AssetBundle;

class MassuploadAsset extends AssetBundle
{
    public $sourcePath = '@app/assets-src/massupload';

    public $css = [
        'css/massupload.css',
    ];

    public $js = [
        'js/massupload.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
