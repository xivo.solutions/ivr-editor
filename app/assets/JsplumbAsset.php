<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class JsplumbAsset extends AssetBundle
{
    public $sourcePath = '@app/assets-src/jsplumb';
    public $css = [
        'css/jsplumb.css',
    ];
    public $js = [
        'js/jquery.jsPlumb-1.6.4-min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
