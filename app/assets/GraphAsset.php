<?php

namespace app\assets;

use yii\web\AssetBundle;

class GraphAsset extends AssetBundle
{
    public $sourcePath = '@app/assets-src/graph';
    public $css = [
        'css/dialplan_base.css',
        'css/dialplan_node.css',
        'css/main.css',
        'css/form.css'
    ];
    public $js = [
        'js/dialplan_base.js',
        'js/dialplan_node.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'app\assets\JsplumbAsset',
        'app\assets\FaAsset'
    ];
}
