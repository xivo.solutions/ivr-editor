<?php

return [
    'start' => 'départ',
    'onhangup' => 'sur raccrocher',
    'onerror' => 'sur erreur',
    'playback' => 'lecture d\'audio', 
    'menu' => 'menu', 
    'hangup' => 'raccrocher',
    'assign' => 'créer une variable', 
    'read' => 'demande de saisie', 
    'saynumber' => 'diction de numéros', 
    'condition' => 'condition', 
    'phone' => 'téléphone',
    'data' => 'données',
    'recstart' => 'début d\'enregistrement',
    'recstop' => 'fin d\'enregistrement',
    'wait' => 'attente',
    'cel' => 'nouveau cel',
    'queue' => 'file d\'attente',
    'http' => 'requête http',
    'goto' => 'destination personnalisée',
    'group' => 'groupe',
    'user' => 'utilisateur',
    'voicemail' => 'boîte vocale'
];
