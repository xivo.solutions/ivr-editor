<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;

app\assets\GraphAsset::register($this);

if (isset($model->id))
    $this->registerJs("var DialplanID = " . $model->id . ";", Yii\web\View::POS_BEGIN);
else
    $this->registerJs("var DialplanID = 0;", Yii\web\View::POS_BEGIN);

$this->registerJs('                                                           
                yii = {                                                                                                     
                    urls: {                                                                                                 
                        load: '. Json::encode(Url::toRoute('/flowchart/load')).',                                   
                        node_create: '. Json::encode(Url::toRoute('/node/create')).',
                        node_delete: '. Json::encode(Url::toRoute('/node/delete')).',
                        node_update: '. Json::encode(Url::toRoute('/node/update')).',
                        node_parameters: '. Json::encode(Url::toRoute('/node/parameters')).',
                        nodecon_create: '. Json::encode(Url::toRoute('/nodecon/create')).',
                        nodecon_delete: '. Json::encode(Url::toRoute('/nodecon/delete')).',
                        nodecon_move: '. Json::encode(Url::toRoute('/nodecon/move')).',
                        nodepos_update: '. Json::encode(Url::toRoute('/nodepos/update')).',
                        
                        
                    }                                                                                                       
                };
');


$this->registerJs('var NodeType = ' . json_encode($types) . ';',  Yii\web\View::POS_BEGIN);
$this->registerJs("dialplan_init();",  Yii\web\View::POS_READY);



?>


<div id="dialplan">
    <?= $this->render('_menu') ?>
    <div id="flowchart-box">
        <div id="flowchart-header">
                <a href="<?php echo Url::toRoute('index'); ?>"><i class="fa fa-sign-out right"></i></a>
                <h1> <?= Yii::t('app', 'Flowchart ')." ". "'" . $model->name . "'" ?></h1>
        </div>
        <div id="flowchart"></div>
    </div>
</div>
