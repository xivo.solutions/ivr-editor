<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Flowchart */


$this->registerCss('/css/xivo.css');

$this->title = Yii::t('app', 'Create Flowchart');

?>
<div class="flowchart-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
