<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Flowchart */
?>

<div class="flowchart-graph">

    <?= $this->render('_graph', [
        'model' => $model,
        'types' => $types
    ]) ?>

</div>
