<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Flowcharts');

//$this->registerCssFile('/css/xivo.css');

?>
<div class="flowchart-index">
    <p>
        <?= Html::a('', ['create'], ['class' => 'glyphicon glyphicon-plus btn btn-default btn-action']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}\n{pager}",
        'columns' => [
            'id',
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->name . '&nbsp;' . Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], [
                        'title' => Yii::t('app', 'Rename'),
                    ]);
                },
                'label' => Yii::t('app', 'Nom'),
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Html::a(Yii::t('app', 'Action'), ['index', 'sort' => 'action'], ['data-sort' => 'action']),
                'template' => '{graph} {duplicate} {delete}',
                'buttons' => [
                    'graph' => function($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-cog"></span>', $url, [
                            'title' => Yii::t('app', 'Configure'),
                        ]);
                    },
                    'duplicate' => function($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-duplicate"></span>', $url, [
                            'title' => Yii::t('app', 'Duplicate'),
                        ]);
                    },
                    'delete' => function($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data' => [
                                'confirm' => Yii::t('app', 'Do you want to delete this flowchart ?'),
                                'method' => 'post',
                            ],
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>

</div>