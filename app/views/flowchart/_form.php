<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<div class="flowchart-form">

    <?php $form = ActiveForm::begin(['action' => Url::current()]); ?>


    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary itb-submit']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
