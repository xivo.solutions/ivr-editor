<?php

use kartik\sidenav\SideNav;

function genMenuItem($type, $level) {
    return [
        'label' => Yii::t('node', $type),
        'template' => '{label}',
        'options' => [ 
            'class' => [ 'node-menu-item', 'menu-item-level-' . $level ],
            'type' => $type,
            'onclick' => 'onclickMenuItem(this)'
        ]
    ];
}

?>

<div id="menu">

<?= SideNav::widget([
    'activateItems' => false,
    'encodeLabels' => true,
    'items' => [
        [ 
            'label' => Yii::t('app', 'Call choices'),
            'active' => true,
            'options' => [ 'class' => [ 'node-menu-item', 'node-menu-section' ]],
            'items' => [
                genMenuItem('menu', 2),
                genMenuItem('user', 2),
                genMenuItem('group', 2),
                genMenuItem('queue', 2),
                genMenuItem('voicemail', 2),
                genMenuItem('playback', 2),
                genMenuItem('hangup', 2),
                genMenuItem('goto', 2),
            ],
        ]
    ]
]) ?>

</div>
