<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Flowchart */

$this->title = Yii::t('app', 'Duplicate Flowchart: {name}', [
    'name' => $orig->name,
]);

?>
<div class="flowchart-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $copy,
    ]) ?>

</div>
