<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

app\assets\PlayfileAsset::register($this);
app\assets\FaAsset::register($this);

?>

<p style="display: inline-block">
<?= Html::a(Yii::t('app', 'Load'), ['createvoiceprompt'], ['class' => 'btn btn-primary itb-submit']) ?></p>
<?= Html::a(Yii::t('app', 'Mass upload'), ['massupload'], ['class' => 'btn btn-primary itb-submit']) ?></p>
<!--
<?= Html::a(Yii::t('app', 'Create using TTS'), ['createtts'], ['class' => 'btn btn-success']) ?></p>
<?= Html::a(Yii::t('app', 'Create from recording'), ['/recorded/indexc'], ['class' => 'btn btn-success']) ?>
-->
</p>

<?= GridView::widget([
	'id'=>'voiceprompt-grid',
	'dataProvider'=> $dataProvider,
        'layout' => "{items}\n{pager}",
	'columns'=> [
		'name',
		'msg',
/*
		'lang',
		'source',
*/
		[
			'class'=>'yii\\grid\\ActionColumn',
			'template' => "{update} {play} {changefile} {delete}",
			'buttons' => [
				'play' => function($url, $model, $key) {
					return Html::a('<i class="fa fa-play" onclick="onclickPlay(event)"></i>', $url , [
					'title' => Yii::t('app', 'Play'),
				    ]);
				},
				'changefile' => function($url, $model, $key) {
					return Html::a('<i class="fa fa-upload"></i>', $url , [
					'title' => Yii::t('app', 'Change file'),
					]);
				},
/* 
				'updatetts' => function($url, $model, $key) {
					return Html::a('<i class="fa fa-file-text-o"></i>', $url);
				}
*/				
			],

			'urlCreator' => function($action, $model, $key, $index, $col) {
				if ($action == 'play')
					return Url::to(['/voiceprompt/play', 'id' => $key, 'time' => time()]);
				else
					return Url::to(['/voiceprompt/' . $action, 'id' => $key]);
			},
			'options' => ['width'=>'80'],
		],
	],
]); 
?>
