<?php

use yii\helpers\Html;

?>

<div class="form">

<?php $form = yii\widgets\ActiveForm::begin([
	'id'=>'voiceprompt-update-form',
	'enableAjaxValidation'=>false,
	'action' => ['update', 'id' => $model->id]
]); ?>

	<p class="note"><?= Yii::t('app', 'Fields with {star} are required', ['star' => '<span class="required">*</span>']) ?></p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->field($model, 'name')->textInput(['size'=>60,'maxlength'=>255]); ?>
	</div>
<!--
	<div class="row">
		<?= $form->field($model, 'source')->label() ?>
		<?= $model->source ?>
	</div>
-->

<?php if ($model->source != 'tts') { ?>
	<div class="row">
		<?= $form->field($model, 'msg')->textArea(); ?>
	</div>
<?php }  ?>

	<div class="row buttons">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btm btn-ptimary itb-submit']); ?>
	</div>

<?php $form->end(); ?>

</div><!-- form -->
