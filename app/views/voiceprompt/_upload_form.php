<?php
use yii\helpers\Html;
?>

<div class="form">

<?php $form = yii\widgets\ActiveForm::begin([
	'id'=>'voiceprompt-upload-form',
	'options' => ['enctype' => 'multipart/form-data'],
	'enableAjaxValidation'=>false,
	'action' => ['changefile', 'id' => $model->id] 
]); ?>
	
	<div class="row" style="margin: 25px 0 10px 0;">
		<?= $form->field($model, 'file')->fileInput(); ?>
	</div>

	<div class="row buttons">
		<?= Html::submitButton('Upload', ['class' => 'btm btn-ptimary itb-submit']); ?>
	</div>
                        	
	

<?php $form->end(); ?>

</div><!-- form -->