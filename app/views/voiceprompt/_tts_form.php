<?php

use yii\helpers\Html;

?>

<div class="form">

<?php $form = yii\widgets\ActiveForm::begin([
	'id'=>'voiceprompt-tts-form',
	'enableAjaxValidation'=>false,
	'action' => $model->isNewRecord ? ['createtts'] : ['updatetts']
]); ?>


	<p class="note"><?= Yii::t('app', 'Fields with {star} are required', ['star' => '<span class="required">*</span>']) ?></p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->field($model, 'name')->textInput(['size'=>60,'maxlength'=>255]); ?>
	</div>
	
	<div class="row">
		<?= $form->field($model, 'msg')->textarea(); ?>
	</div>
	
	<div class="row">
		<?= $form->field($model, 'lang')->listBox(app\models\Voiceprompt::languages(), ['size' => 1, 'prompt' => '- Select language -']); ?>
	</div>
	
	<div class="row">
		<?= $form->field($model, 'speed')->textInput(['size'=>5,'maxlength'=>5]); ?>
	</div>
	

	<div class="row buttons">
		<?= Html::submitButton($model->isNewRecord ? Yii:t('app', 'Create') : Yii::t('app', 'Update'); ?>
	</div>

<?php $form->end(); ?>

</div><!-- form -->
