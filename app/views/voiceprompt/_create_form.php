<?php
use yii\helpers\Html;
?>

<div class="form">

<?php $form = yii\widgets\ActiveForm::begin([
	'id'=>'voiceprompt-create-form',
	'options' => ['enctype' => 'multipart/form-data'],
	'enableAjaxValidation'=>false,
	'action' => ['createvoiceprompt'] 
]); ?>

	<p class="note"><?= Yii::t('app', 'Fields with {star} are required', ['star' => '<span class="required">*</span>']) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->field($model, 'name')->textInput(['size'=>60,'maxlength'=>255]); ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'msg')->textarea(['width'=>255,'height'=>10]); ?>
	</div>
	
	<div class="row">
		<?= $form->field($model, 'file')->fileInput(); ?>
	</div>

	<div class="row buttons">
		<?= Html::submitButton('Upload', ['class' => 'btn btn-primary itb-submit']); ?>
	</div>
                        	
	

<?php $form->end(); ?>

</div><!-- form -->