<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;

app\assets\MassuploadAsset::register($this);
?>

<div class="form">

<div id='errors' style="color: red; font-weight: bold">
<ul></ul>
</div>


<?= GridView::widget([
	'id'=>'voiceprompt-massupload-grid',
	'dataProvider' => new ArrayDataProvider(['allModels' => []]),
        'layout' => "{items}",
        'emptyText' => Yii::t('app', 'No files selected'),
	'columns'=> [
		[ 
			'header' => Yii::t('app', 'File name'),
			'headerOptions' => [ 'style' => 'width: 40%' ]
		],
		[ 
			'header' => Yii::t('app', 'Voice prompt name'),
			'headerOptions' => [ 'style' => 'width: 40%' ]
		],
		[
			'header' => Yii::t('app', 'Delete'),
			'headerOptions' => [ 'style' => 'width: 10%' ]
		],
		[ 
			'header' => Yii::t('app', 'Result'),
			'headerOptions' => [ 'style' => 'width: 10%' ]
		],
	],
]);
?>

<?php $form = yii\widgets\ActiveForm::begin([
	'id'=>'voiceprompt-massupload-form',
	'options' => ['enctype' => 'multipart/form-data'],
	'enableAjaxValidation'=>false,
	'action' => ['massupload'] 
]); ?>

	<div class="row buttons">
		<?= Html::button(Yii::t('app', 'Select files'), ['id' => 'button-select']); ?>
	</div>

	<div class="row" style="margin: 25px 0 10px 0;">
		<?= Html::fileInput('dummy', null, [
			'style' => 'display: none',
			'multiple' => true,
			'accept' => 'audio/*',
			'id' => 'fileinput'
		]); ?>
	</div>

	<div class="row buttons">
		<?= Html::button(Yii::t('app', 'Upload'), ['id' => 'button-upload', 'class' => 'btn btn-primary itb-submit']); ?>
	</div>
                        	
<?php $form->end(); ?>


<script>
submitUrl="<?= Url::to(['createvoiceprompt']) ?>";
indexUrl="<?= Url::to(['index']) ?>";
</script>

</div><!-- form -->
