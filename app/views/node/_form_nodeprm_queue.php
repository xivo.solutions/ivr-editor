<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use app\models\FormNodeprm;
use app\models\Queuefeatures;

?>


<?php
	$seldata = ArrayHelper::map(Queuefeatures::find()->all(), 'name', 'name');
?>

<div class="form">

<?= $this->render('_form_head', ['model' => $model ]) ?>

<?php $form = ActiveForm::begin([
	'options' => [
		'id' => 'form-node-prm',
		'onSubmit' => 'return submitNodePrmForm()',
	],
	'action' => false
]); ?>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->field($model, 'name')->dropDownList($seldata, ['size' => 1, 'prompt' => 'Select queue']); ?>
	</div>

	<div class="row buttons">
		<?= Html::SubmitButton(Yii::t('app', 'Update')) ?>
		<?= Html::Button(Yii::t('app', 'Cancel'), ['onclick' => "closeForm('div-node-prm-form')"]); ?>
	</div>

<?php ActiveForm::end(); ?>

</div>
