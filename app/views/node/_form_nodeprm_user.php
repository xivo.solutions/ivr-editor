<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use app\models\FormNodeprm;
use app\models\Userfeatures;
use kartik\typeahead\Typeahead;

$script_set_fields = <<<SCRIPT
function set_fields(obj) {
	$("#formnodeprmuser-id").val(obj.id);
	$("#formnodeprmuser-name").val(obj.name);
	$("#formnodeprmuser-number").val(obj.number);
}
SCRIPT;

$this->registerJs($script_set_fields);

$name = '';
$number = '';
if ($extra_data) {
	$name = $extra_data->firstname . ' ' . $extra_data->lastname;
	$number = $extra_data->lines[0]->number;
}

?>




<div class="form">

<h3><?= Yii::t('app', 'Node parameters') ?></h3>

<?php $form = ActiveForm::begin([
	'options' => [
		'id' => 'form-node-prm',
		'onSubmit' => 'return submitNodePrmForm()',
	],
	'action' => false
]); ?>
	<p class="note"><?= Yii::t('app', 'Fields with {star} are required', ['star' => '<span class="required">*</span>']) ?></p>

	<?= $form->errorSummary($model); ?>

	<?= $form->field($model, 'id')->hiddenInput()->label(false); ?>

	<label class="control-label"><?= Yii::t('app', 'First and Last Name') ?></label>
	<div class="row">
		<?= Typeahead::widget([
			'name' => 'user',
			'value' => $name,
			'id' => 'formnodeprmuser-name',
			'options' => ['placeholder' => Yii::t('app', 'Type min {num} letters ...', ['num' => 3])],
			'pluginOptions' => ['highlight'=>true, 'minLength' => 3],
			'pluginEvents' => [
				'typeahead:select' => 'function(ev, obj) { set_fields(obj); }',
				'typeahead:autocomplete' => 'function(ev, obj) { set_fields(obj); }',
			],
			'dataset' => [
				[
					'remote' => [
						'url' => Url::to(['user/usersearch']) . '?q=%QUERY',
						'wildcard' => '%QUERY'
					],
					'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('name')",
					'display' => 'name',
					'limit' => 10
				]
			]
		]) ?>
	</div>

	<label class="control-label"><?= Yii::t('app', 'Internal Number') ?></label>
	<div class="row">
		<?= Typeahead::widget([
			'name' => 'number',
			'value' => $number,			
			'id' => 'formnodeprmuser-number',
			'options' => ['placeholder' => Yii::t('app', 'Type min {num} digits ...', ['num' => 3])],
			'pluginOptions' => ['highlight'=>true, 'minLength' => 3],
			'pluginEvents' => [
				'typeahead:select' => 'function(ev, obj) { set_fields(obj); }',
				'typeahead:autocomplete' => 'function(ev, obj) { set_fields(obj); }',
			],
			'dataset' => [
				[
					'remote' => [
						'url' => Url::to(['user/numbersearch']) . '?q=%QUERY',
						'wildcard' => '%QUERY'
					],
					'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('number')",
					'display' => 'number',
					'limit' => 10
				]
			]
		]) ?>
	</div>

	<div class="row buttons">
		<?= Html::SubmitButton(Yii::t('app', 'Update')) ?>
		<?= Html::Button(Yii::t('app', 'Cancel'), ['onclick' => "closeForm('div-node-prm-form')"]); ?>
	</div>

<?php ActiveForm::end(); ?>

</div>
