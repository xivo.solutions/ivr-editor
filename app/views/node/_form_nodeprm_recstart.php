<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>


<div class="form">

<?= $this->render('_form_head', ['model' => $model ]) ?>

<?php $form=ActiveForm::begin(['options' => ['id' => 'form-node-prm', 'onSubmit' => 'return submitNodePrmForm()'], 'action' => false]); ?>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->field($model, 'file')->textInput(['size'=>50, 'maxlength'=>255]); ?>
	</div>


	<div class="row">
		<?= $form->field($model, 'bridged')->checkBox(); ?>
	</div>
	<div class="row">
		<?= $form->field($model, 'volin')->textInput(['size'=>5]); ?>
	</div>
	
	<div class="row">
		<?= $form->field($model, 'volout')->textInput(['size'=>5]); ?>
	</div>


	<div class="row buttons">
		<?= Html::SubmitButton(Yii::t('app', 'Update')) ?>
		<?= Html::Button(Yii::t('app', 'Cancel'), array('onclick' => "closeForm('div-node-prm-form')") ); ?>
	</div>

<?php ActiveForm::end(); ?>


</div>
