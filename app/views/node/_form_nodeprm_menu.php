<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\FormNodeprm;
use app\components\VoicePromptFile;

?>


<?php

	$seldata = VoicePromptFile::listAll('vp', 1);
	foreach(['vp_menu', 'vp_invalid', 'vp_nochoice'] as $idx)
		$sw[$idx] = !FormNodeprm::isExpression($model->{$idx});
		
?>

<div class="form">

<?= $this->render('_form_head', ['model' => $model ]) ?>

<?php $form=ActiveForm::begin([
	'fieldClass' => 'app\components\MyActiveField',
	'options' => [
		'id' => 'form-node-prm',
		'onSubmit' => 'return submitNodePrmForm()'
	],
	'action' => false
]); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->field($model, 'repeat')->textInput(['size' => 20]) ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'timeout')->textInput(['size' => 20]) ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'vp_menu')->vpInput($seldata, $sw['vp_menu'], ['size' => 1, 'empty' => '-'], ['size' => 20]); ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'vp_nochoice')->vpInput($seldata, $sw['vp_nochoice'], ['size' => 1, 'empty' => '-'], ['size' => 20]); ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'vp_invalid')->vpInput($seldata, $sw['vp_invalid'], ['size' => 1, 'empty' => '-'], ['size' => 20]); ?>
	</div>

	<div class="row buttons">
		<?= Html::SubmitButton(Yii::t('app', 'Update')) ?>
		<?= Html::Button(Yii::t('app', 'Cancel'), array('onclick' => "closeForm('div-node-prm-form')") ); ?>
	</div>

<?php ActiveForm::end(); ?>

</div>



