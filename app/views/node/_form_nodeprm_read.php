<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\FormNodeprm;
use app\components\VoicePromptFile;


	$seldata = VoicePromptFile::listAll('vp', 1);
	foreach(array('vp_file', 'vp_invalid') as $idx)
		$sw[$idx] = !FormNodeprm::isExpression($model->{$idx});
?>

<div class="form">

<?= $this->render('_form_head', ['model' => $model ]) ?>

<?php $form=ActiveForm::begin([
	'fieldClass' => 'app\components\MyActiveField',
	'options' => [
		'id' => 'form-node-prm',
		'onSubmit' => 'return submitNodePrmForm()'
	],
	'action' => false
]); ?>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->field($model, 'repeat')->textInput(['size' => 20]) ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'vp_file')->vpInput($seldata, $sw['vp_file'], ['size' => 1, 'empty' => '-'], ['size' => 20]); ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'vp_invalid')->vpInput($seldata, $sw['vp_invalid'], ['size' => 1, 'empty' => '-'], ['size' => 20]); ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'beep')->checkBox(); ?>
	</div>
	

	<div class="row">
		<?= $form->field($model, 'min_digits')->textInput(['size'=>20, 'maxlength'=>255]); ?>
	</div>
	
	<div class="row">
		<?= $form->field($model, 'max_digits')->textInput(['size'=>20, 'maxlength'=>255]); ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'numeric')->checkBox(); ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'min')->textInput(['size'=>20, 'maxlength'=>255]); ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'max')->textInput(['size'=>20, 'maxlength'=>255]); ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'var')->textInput(['size'=>20, 'maxlength'=>255]); ?>
	</div>


	<div class="row buttons">
		<?= Html::SubmitButton(Yii::t('app', 'Update')) ?>
		<?= Html::Button(Yii::t('app', 'Cancel'), ['onclick' => "closeForm('div-node-prm-form')"]); ?>
	</div>

<?php ActiveForm::end(); ?>

</div>
