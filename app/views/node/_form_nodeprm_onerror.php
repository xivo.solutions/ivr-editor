<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="form">

<?= $this->render('_form_head', ['model' => $model ]) ?>

<?= Yii::t('app', 'This node type has no parameters') ?>

<?php $form=ActiveForm::begin(); ?>
	<div class="row buttons">
		<?= Html::Button(Yii::t('app', 'Cancel'), ['onclick' => "closeForm('div-node-prm-form')"] ); ?>
	</div>

<?php ActiveForm::end(); ?>


</div>
