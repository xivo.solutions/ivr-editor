<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use kartik\typeahead\Typeahead;

$script_set_fields = <<<SCRIPT
function set_fields(obj) {
	$("#formnodeprmvoicemail-uniqueid").val(obj.uniqueid);
	$("#formnodeprmvoicemail-fullname").val(obj.fullname);
	$("#formnodeprmvoicemail-mailbox").val(obj.mailbox);
}
SCRIPT;

$this->registerJs($script_set_fields);

$fullname = '';
$mailbox = '';
// extr_data - app\model\Voicemail
if ($extra_data) {
	$fullname = $extra_data->fullname;
	$mailbox = $extra_data->mailbox;
}

?>

<div class="form">

<h3><?= Yii::t('app', 'Node parameters') ?></h3>

<?php $form = ActiveForm::begin([
	'options' => [
		'id' => 'form-node-prm',
		'onSubmit' => 'return submitNodePrmForm()',
	],
	'action' => false
]); ?>
	<p class="note"><?= Yii::t('app', 'Fields with {star} are required', ['star' => '<span class="required">*</span>']) ?></p>

	<?= $form->errorSummary($model); ?>

	<?= $form->field($model, 'uniqueid')->hiddenInput()->label(false); ?>

	<label class="control-label"><?= Yii::t('app', 'Full name') ?></label>
	<div class="row">
		<?= Typeahead::widget([
			'name' => 'fullname',
			'value' => $fullname,
			'id' => 'formnodeprmvoicemail-fullname',
			'options' => ['placeholder' => Yii::t('app', 'Type min {num} letters ...', ['num' => 3])],
			'pluginOptions' => ['highlight'=>true, 'minLength' => 3],
			'pluginEvents' => [
				'typeahead:select' => 'function(ev, obj) { set_fields(obj); }',
				'typeahead:autocomplete' => 'function(ev, obj) { set_fields(obj); }',
			],
			'dataset' => [
				[
					'remote' => [
						'url' => Url::to(['voicemail/namesearch']) . '?q=%QUERY',
						'wildcard' => '%QUERY'
					],
					'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('fullname')",
					'display' => 'fullname',
					'limit' => 10
				]
			]
		]) ?>
	</div>

	<label class="control-label"><?= Yii::t('app', 'Voicemail number') ?></label>
	<div class="row">
		<?= Typeahead::widget([
			'name' => 'mailbox',
			'value' => $mailbox,
			'id' => 'formnodeprmvoicemail-mailbox',
			'options' => ['placeholder' => Yii::t('app', 'Type min {num} digits ...', ['num' => 3])],
			'pluginOptions' => ['highlight'=>true, 'minLength' => 3],
			'pluginEvents' => [
				'typeahead:select' => 'function(ev, obj) { set_fields(obj); }',
				'typeahead:autocomplete' => 'function(ev, obj) { set_fields(obj); }',
			],
			'dataset' => [
				[
					'remote' => [
						'url' => Url::to(['voicemail/mailboxsearch']) . '?q=%QUERY',
						'wildcard' => '%QUERY'
					],
					'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('mailbox')",
					'display' => 'mailbox',
					'limit' => 10
				]
			]
		]) ?>
	</div>

	<div class="row buttons">
		<?= Html::SubmitButton(Yii::t('app', 'Update')) ?>
		<?= Html::Button(Yii::t('app', 'Cancel'), ['onclick' => "closeForm('div-node-prm-form')"]); ?>
	</div>

<?php ActiveForm::end(); ?>

</div>
