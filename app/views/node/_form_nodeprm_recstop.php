<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>

<div class="form">

<?= $this->render('_form_head', ['model' => $model ]) ?>

<?= Yii::t('app', 'This node type has no parameters') ?>

<?php $form=ActiveForm::begin(['options' => ['id' => 'form-node-prm', 'onSubmit' => 'return submitNodePrmForm()'], 'action' => false]); ?>
	<div class="row buttons">
		<?= Html::Button(Yii::t('app', 'Cancel'), array('onclick' => "closeForm('div-node-prm-form')") ); ?>
	</div>

<?php ActiveForm::end(); ?>


</div>
