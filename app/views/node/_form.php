<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
	'action' => false,
	'options' => [
		'id' => 'form-node-name',
		'onSubmit' => 'return submitNodeNameForm()'
	]
]); ?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>

	<div class="form-group">
		<?= Html::SubmitButton(Yii::t('app', 'Update')); ?>
		<?= Html::Button(Yii::t('app', 'Cancel'), array('onclick' => "closeForm('div-node-name-form')") ); ?>
	</div>
<?php ActiveForm::end(); ?>

</div>
