<?php

use Yii;

use yii\helpers\Html;
use yii\helpers\BaseArrayHelper;
use yii\widgets\ActiveForm;

use app\models\Phone;

	$seldata = BaseArrayHelper::map(Phone::FindAll(['id', 'name');
?>

<div class="form">

<?= $this->render('_form_head', ['model' => $model ]) ?>

<?php $form=ActiveForm::begin(['options' => ['id' => 'form-node-prm', 'onSubmit' => 'return submitNodePrmForm()'], 'action' => false]); ?>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->field($model, 'id_phone')->listBox($seldata,  ['size' => 1]); ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'number')->textInput(['size' => 20, 'maxlength' => 255]); ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'timeout')->textInput(['size'=>5, 'maxlength'=>255]); ?>
	</div>

	<div class="row">
		<?= $form->field($model, 'timeout')->checkBox(); ?>
	</div>
	
	<div class="row buttons">
		<?= Html::SubmitButton(Yii::t('app', 'Update')) ?>
		<?= Html::Button(Yii::t('app', 'Cancel'), array('onclick' => "closeForm('div-node-prm-form')") ); ?>
	</div>

<?php ActiveForm::end(); ?>


</div>
