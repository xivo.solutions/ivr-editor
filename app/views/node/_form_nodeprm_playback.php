<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\FormNodeprm;
use app\components\VoicePromptFile;


?>


<?php

	$seldata = VoicePromptFile::listAll('vp', 1);
	$sw = !FormNodeprm::isExpression($model->vp_file);
?>

<div class="form">

<?= $this->render('_form_head', ['model' => $model ]) ?>

<?php $form = ActiveForm::begin([
	'fieldClass' => 'app\components\MyActiveField',
	'options' => [
		'id' => 'form-node-prm',
		'onSubmit' => 'return submitNodePrmForm()',
	],
	'action' => false,
	
]); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->field($model, 'vp_file')->vpInput($seldata, $sw, ['size' => 1, 'empty' => '-'], ['size' => 20]); ?>
	</div>

	<div class="row buttons">
		<?= Html::SubmitButton(Yii::t('app', 'Update')) ?>
		<?= Html::Button(Yii::t('app', 'Cancel'), array('onclick' => "closeForm('div-node-prm-form')") ); ?>
	</div>

<?php ActiveForm::end(); ?>


</div>
