<?php

use Yii;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use app\models\Groupfeatures;

?>

<?php
	$seldata = ArrayHelper::map(Groupfeatures::find()->all(), 'name', 'name');
?>



<div class="form">

<h3><?= Yii::t('app', 'Node parameters') ?></h3>

<?php $form=ActiveForm::begin(['options' => ['id' => 'form-node-prm', 'onSubmit' => 'return submitNodePrmForm()'], 'action' => false]); ?>
	<p class="note"><?= Yii::t('app', 'Fields with {star} are required', ['star' => '<span class="required">*</span>']) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?= $form->field($model, 'grpname')->dropDownList($seldata, ['size' => 1, 'prompt' => 'Select group']); ?>
	</div>

	<div class="row buttons">
		<?= Html::SubmitButton(Yii::t('app', 'Update')) ?>
		<?= Html::Button(Yii::t('app', 'Cancel'), array('onclick' => "closeForm('div-node-prm-form')") ); ?>
	</div>

<?php ActiveForm::end(); ?>

</div>
