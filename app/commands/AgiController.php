<?php

namespace app\commands;

use yii\console\Controller;
use app\components\AGI;
use app\components\Dialplan;
use app\components\menuNode;

class AgiController extends Controller {
  
  public function actionIndex() {
    $agi = new AGI();
    
    $agi->read_agi_variables();

    $agi->cmd_set_variable("AGISIGHUP", "no");

    $script = $agi->get_agi_var("agi_network_script");
    
    return $this->runAction($script, [$agi]);
  }
  
  
  public function actionDialplan($agi) {
    
    $id_flowchart = $agi->get_agi_var("agi_arg_1");

    $dialplan = Dialplan::load($id_flowchart);
    
    if ($dialplan) {
      try {
        $ret = $dialplan->run($agi);
        if ($ret == 0)
          return 0;
      } catch (Exception $e) {
        $agi->cmd_set_variable("XIVO_IVR_ERROR", "IVR agi exception: " . $e->getMessage());
      }
    } else {
      $msg = "IVR dialplan id {$id_flowchart} not found";
      $agi->cmd_set_variable("XIVO_IVR_ERROR", $msg);
      $agi->cmd_verbose($msg);
    }

    $agi->exec_goto("", "failed", 1);
    return 1;
  }

  public function actionTest() {
    $menu = new menuNode(7);
    $cons = $menu->connectedOpts();
    print_r($cons);
  }
}