<?php

$user_account = null;
$settings = null;

function account_id() {
  global $user_account;
  return isset($user_account) ? $user_account->id : null;
}

function account_name() {
  global $user_account;
  return isset($user_account) ? $user_account->name : null;
}

function settings($name) {
  global $settings;
  
  return isset($settings) && isset($settings->{$name}) ? $settings->{$name} : null;
}