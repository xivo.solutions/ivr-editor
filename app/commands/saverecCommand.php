<?php

class saverecCommand extends CConsoleCommand {
  
  private $id;
  
  public function run($args) {

    $agi = new AGI();
    
    $agi->read_agi_variables();

    $account = Account::model()->findByAttributes(array('name' => $args[0]));
    
    if (!$account)
      return 1;

    if (!($rec = VoicePromptFile::mkUnique($account->name, 'rec')))
      return 1;

    echo ($rec->getPath() . "\n");
    echo ($args[1] . "\n");
    
    if (!file_exists($args[1]))
      return 1;
          
    $rec->save($args[1]);
    return 0;    
  }
}
