<?php

return [
    'clientSoundDir' => '/var/lib/xivo/ivrsounds',
    'clientMonitorDir' => '/var/db/cc/monitor',

    'disabled_nodes' => ['recstart', 'recstop', 'data', 'phone'],
];
