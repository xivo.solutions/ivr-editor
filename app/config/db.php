<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=db;dbname=asterisk',
    'username' => '%DB_USER%',
    'password' => '%DB_PASSWORD%',
    'charset' => 'utf8',

    'enableSchemaCache' => true,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache',
];
