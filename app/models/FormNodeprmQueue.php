<?php

namespace app\models;

class FormNodeprmQueue extends FormNodeprm {
  public $name;

  public function runtimeRules() {
    return [
      [['name'], 'exist', 'targetClass' => Queuefeatures::class]
    ];
  }

  public function attributeLabels() {
    return [
	'name' => \Yii::t('app', 'Name'),
    ];
  }

  public function genName() {
    return self::normalizeName($this->name, false);
  }

}  
