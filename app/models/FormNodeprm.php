<?php

namespace app\models;

use yii\base\Exception;

use app\components\VariantValidator;
use app\components\Expression;


class FormNodeprm extends PrmForm {

    const VAR_PATTERN = '/^\$[[:alpha:]_][\d\w_]*$/';
    const EXPR_PATTERN = '/^=/';

    const SC_RUNTIME = 'runtime';

    const NAME_UNKNOWN = '<unset>';


    public static function recordClass() {
        return __NAMESPACE__ . '\\Nodeprm';
    }

    public static function nameAttribute() {
        return 'prm_name';
    }

    public static function valueAttribute() {
        return 'prm_value';
    }

    public static function selectAttribute() {
        return 'id_node';
    }

    public function exprAttributes() {
        return [];
    }

    public function runtimeRules() {
        return [];
    }

    public static function isExpression($str) {
        return is_string($str) && preg_match(self::EXPR_PATTERN, $str);
    }

    public static function formClass($type) {
        if (!is_string($type) || !$type)
            return null;
            
        $type = ucfirst($type);
        
        $class = __NAMESPACE__ . "\\FormNodeprm" . $type;
        return class_exists($class) ? $class : null;
    }

    public static function nodeType() {
        return strtolower(substr(get_called_class(), strlen(__NAMESPACE__) + 1 + strlen('FormNodeprm')));
    }


    public function evaluate($user_variables) {
        foreach($this->exprAttributes() as $a) {
            if (self::isExpression($this->{$a})) {
                try {
                    $e = new Expression(substr($this->{$a},1));
                    $this->{$a} = $e->evaluate($user_variables);
                } catch (Exception $e) {
                    return false;
                }
            }
        }
        
        return true;
    }


   public function scenarios() {
        $sc = parent::scenarios();
        if (!in_array(self::SC_RUNTIME, array_keys($sc))) {
            $sc[self::SC_RUNTIME] = [];
        }
        
        return $sc;
    }

    public function rules() {
        $rules = [];
        $exprattr = $this->exprAttributes();
        
        $exprrules = [];
        foreach ($exprattr as $ea) {
            $exprrules[$ea] = [];
        }
        
        foreach ($this->runtimeRules() as $r) {
            $aa = array_shift($r);
            if (!is_array($aa))
                $aa = [$aa];
                        
            foreach ($aa as $a) {
                if (in_array($a, $exprattr)) {
                    // create runtime rule
                    $r2 = $r;
                    $r2['on'] = [self::SC_RUNTIME];
                    array_unshift($r2, [$a]);
                    array_push($rules, $r2);
                    
                    // and now add insert rule to list
                    $r2 = $r;
                    array_push($exprrules[$a], $r2);
                } else {
                    $r2 = $r;
                    array_unshift($r2, [$a]);
                    array_push($rules, $r2);
                }
            }
        }    


        // create variant validators for expr attributes
            
        foreach ($exprrules as $a => $rlist) {
            $vrule = [[$a], 'app\\components\\VariantValidator', 'decision' => self::EXPR_PATTERN,
                'rules1' => [
                    ['app\components\ExprValidator']
                ],
                'rules2' => $rlist,
            ];
            
            array_push($rules, $vrule);
        }

        return $rules;
    }

    public function viewExtraData() {
        return null;
    }

    protected static function normalizeName($name, $expr = true) {
        if (!$name)
            return '';
        
        if ($expr && self::isExpression($name))
            return '(' . trim(substr($name, 1)) . ')';
        else
            return trim($name);
    }

    public function genName() {
        return ucfirst(self::nodeType());
    }
    
    public function automaticName() {
        $name = $this->genName();
        return $name ? $name : self::NAME_UNKNOWN;
    }
    
}  
