<?php

namespace app\models;

class FormNodeprmUser extends FormNodeprm {
  public $id;

  public function runtimeRules() {
    return [
      [['id'], 'exist', 'targetClass' => Userfeatures::class]
    ];
  }

  public function attributeLabels() {
    return [
	'id' => \Yii::t('app', 'User ID'),
    ];
  }

  public function viewExtraData() {
    $user = Userfeatures::find()->where(['id' => $this->id])->with('lines')->one();
    if (!$user || empty($user->lines))
      return null;
    else
      return $user;
  }

  public function genName() {
    $user = Userfeatures::findOne($this->id);
    if (!$user)
      return '';
    return self::normalizeName($user->firstname . " " . $user->lastname, false);
  }
}

