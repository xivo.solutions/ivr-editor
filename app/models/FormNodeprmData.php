<?php

namespace app\models;

class FormNodeprmData extends FormNodeprm {
  public $command;
  public $variables;
  
  public function rules() {
    return [
      [['command'], 'match', 'pattern' => '/^[\w_-]+$/'],
      [['variables'], 'ListValidator', 'delimiter' => '/\s+/' ,'rules' => [
      [['match'], 'allowEmpty' => false, 'pattern' => FormNodeprm::VAR_PATTERN]
      ]],
    ];
  }

  public function attributeLabels() {
    return [
      'command' => \Yii::t('app', 'Command'),
      'variables' => \Yii::t('app', 'Variables'),
    ];
  }

}  
