<?php

namespace app\models;

class FormNodeprmSaynumber extends FormNodeprm {
  public $number;

  public function exprAttributes() {
    return ['number'];
  }
  
  public function runtimeRules() {
    return [
      [['number'], 'integer']
    ];
  }


  public function attributeLabels() {
    return [
      'number' => \Yii::t('app', 'Number'),
    ];
  }

  public function genName() {
    return self::normalizeName($this->number);
  }
}  
