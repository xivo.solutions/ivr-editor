<?php

namespace app\models;

class FormNodeprmWait extends FormNodeprm {
  public $seconds;

  public function exprAttributes() {
    return ['seconds'];
  }
  
  public function runtimeRules() {
    return [
      [['seconds'], 'integer'],
      [['seconds'], 'required'],
    ];
  }


  public function attributeLabels() {
    return [
      'seconds' => \Yii::t('app', 'Seconds'),
    ];
  }


  public function genName() {
    return self::normalizeName($this->seconds);
  }


}  
