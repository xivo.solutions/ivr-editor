<?php

namespace app\models;

class FormNodeprmRead extends FormNodeprm {
	public $vp_file;
	public $vp_invalid;
	public $repeat;
	public $min_digits;
	public $max_digits;
	public $numeric; 
	public $min;
	public $max;
	public $beep;
	public $var;


	public function exprAttributes() {
                return ['vp_file','vp_invalid', 'repeat', 'min_digits', 'max_digits', 'min', 'max'];
	}

	public function runtimeRules() {
		return [
			[['vp_file','var'], 'required'],
			[['vp_file','vp_invalid'], 'app\components\VoicePromptFileValidator'],
			[['repeat'], 'integer', 'min' => 0, 'max' => 10],
			[['min_digits','max_digits'], 'integer', 'min' => 0, 'max' => 20],
			[['min', 'max'], 'integer', 'min' => 0],
			[['beep'], 'integer', 'min' => 0, 'max' => 1],
			[['numeric'], 'integer', 'min' => 0, 'max' => 1],
			[['var'], 'match', 'pattern' => self::VAR_PATTERN],
		];
	}


	public function attributeLabels() {
		return [
			'vp_file' => \Yii::t('app', 'Voice prompt file'),
			'vp_invalid' => \Yii::t('app', 'Invalid voice prompt'),
			'repeat' => \Yii::t('app', 'Repeat'),
			'min_digits' => \Yii::t('app', 'Min digits'),
			'max_digits' => \Yii::t('app', 'Max digits'),
			'numeric' => \Yii::t('app', 'Numeric'),
			'min' => \Yii::t('app', 'Min value'),
			'max' => \Yii::t('app', 'Max value'),
			'beep' => \Yii::t('app', 'Beep'),
			'var' => \Yii::t('app', 'Variable'),
		];
  	}

  	public function genName() {
		return self::normalizeName($this->var, false));
	}
}
