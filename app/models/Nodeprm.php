<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nodeprm".
 *
 * @property int $id
 * @property int $id_node
 * @property string $prm_name
 * @property string $prm_value
 *
 * @property Node $node
 */
class Nodeprm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ivr.ivredit_nodeprm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prm_value'], 'default', 'value' => ''],
            [['id_node', 'prm_name'], 'required'],
            [['id_node'], 'default', 'value' => null],
            [['id_node'], 'integer'],
            [['prm_name'], 'string', 'max' => 50],
            [['prm_value'], 'string', 'max' => 255],
            [['id_node'], 'exist', 'skipOnError' => true, 'targetClass' => Node::className(), 'targetAttribute' => ['id_node' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_node' => Yii::t('app', 'Id Node'),
            'prm_name' => Yii::t('app', 'Prm Name'),
            'prm_value' => Yii::t('app', 'Prm Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNode()
    {
        return $this->hasOne(Node::className(), ['id' => 'id_node']);
    }

}
