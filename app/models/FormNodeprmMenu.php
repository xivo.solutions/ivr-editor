<?php

namespace app\models;

class FormNodeprmMenu extends FormNodeprm {
  public $repeat;
  public $timeout;
  public $vp_menu;
  public $vp_nochoice;
  public $vp_invalid;
  
  public function exprAttributes() {
    return ['repeat', 'timeout', 'vp_menu', 'vp_nochoice', 'vp_invalid'];
  }
  
  public function runtimeRules() {
    return [
      [['repeat'], 'integer', 'min' => 0, 'max' => 10],
      [['timeout'], 'integer', 'min' => 2, 'max' => 300],
      [['vp_menu','vp_nochoice','vp_invalid'], 'app\components\VoicePromptFileValidator'],
    ];
  }
  
  public function attributeLabels() {
    return [
      'repeat' => \Yii::t('app', 'Repeat'),
      'timeout' => \Yii::t('app', 'Timeout'),
      'vp_menu' => \Yii::t('app', 'Menu voiceprompt'),
      'vp_invalid' => \Yii::t('app', 'Outdated attempt voiceprompt'),
      'vp_nochoice' => \Yii::t('app', 'No choice or invalid voiceprompt'),
    ];
  }

  public function genName() {
    return self::normalizeName($this->vp_menu);
  }
}  
