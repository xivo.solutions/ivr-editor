<?php

namespace app\models;

use Yii;

class Nodecon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ivr.ivredit_nodecon';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_flowchart', 'src', 'src_opt', 'dst'], 'required'],
            [['id_flowchart', 'src', 'dst'], 'default', 'value' => null],
            [['id_flowchart', 'src', 'dst'], 'integer'],
            [['src_opt'], 'string', 'max' => 20],
            [['id_flowchart', 'src', 'src_opt'], 'unique', 'targetAttribute' => ['id_flowchart', 'src', 'src_opt']],
            [['id_flowchart', 'src'], 'exist', 'skipOnError' => true, 'targetClass' => Node::className(), 'targetAttribute' => ['id_flowchart' => 'id_flowchart', 'src' => 'node_number']],
            [['id_flowchart', 'dst'], 'exist', 'skipOnError' => true, 'targetClass' => Node::className(), 'targetAttribute' => ['id_flowchart' => 'id_flowchart', 'dst' => 'node_number']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlowchart()
    {
        return $this->hasOne(Node::className(), ['id_flowchart' => 'id_flowchart', 'node_number' => 'src']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlowchart0()
    {
        return $this->hasOne(Node::className(), ['id_flowchart' => 'id_flowchart', 'node_number' => 'dst']);
    }

}
