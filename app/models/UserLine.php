<?php

namespace app\models;

use Yii;

class Userline extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_line';
    }

    public function getLines()
    {
        return Linefeatures::hasMany(['id' => 'id_line']);
    }

    public function getUsers()
    {
        return Userfeatures::hasMany(['id' => 'id_user']);
    }
}
