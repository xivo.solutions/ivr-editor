<?php

namespace app\models;


class FormNodeprmAssign extends FormNodeprm {
  public $var;
  public $expr;

  public function exprAttributes() {
    return ['expr'];
  }   

  public function runtimeRules() {
    return [
      [['var'], 'required'],
      [['var'], 'match', 'pattern' => self::VAR_PATTERN],
    ];
  }

  public function attributeLabels() {
    return [
      'var' => \Yii::t('app', 'Variable'),
      'expr' => \Yii::t('app', 'Expression'),
    ];
  }
  
  public function genName() {
    return self::normalizeName($this->var, false);
  }
}  
