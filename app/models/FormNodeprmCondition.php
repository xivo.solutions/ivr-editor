<?php

namespace app\models;

class FormNodeprmCondition extends FormNodeprm {
  public $cond;

  public function exprAttributes() {
    return ['cond'];
  }

  public function attributeLabels() {
    return [
      'cond' => \Yii::t('app', 'Condition'),
    ];
  }

  public function genName() {
    return normalizeName($this->cond);
  }
}
