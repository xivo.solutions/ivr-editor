<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nodepos".
 *
 * @property int $id_node
 * @property int $x
 * @property int $y
 *
 * @property Node $node
 */
class Nodepos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ivr.ivredit_nodepos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_node'], 'required'],
            [['id_node', 'x', 'y'], 'default', 'value' => null],
            [['id_node', 'x', 'y'], 'integer'],
            [['id_node'], 'unique'],
            [['id_node'], 'exist', 'skipOnError' => true, 'targetClass' => Node::className(), 'targetAttribute' => ['id_node' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNode()
    {
        return $this->hasOne(Node::className(), ['id' => 'id_node']);
    }

}
