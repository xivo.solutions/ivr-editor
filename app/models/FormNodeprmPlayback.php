<?php

namespace app\models;

class FormNodeprmPlayback extends FormNodeprm {
  public $vp_file;

  public function exprAttributes() {
    return ['vp_file'];
  }

  public function runtimeRules() {
    return [
      [['vp_file'], 'app\components\VoicePromptFileValidator']
    ];
  }

  public function attributeLabels() {
    return [
	'vp_file' => \Yii::t('app', 'Voice prompt file'),
    ];
  }

  public function genName() {
    return self::normalizeName($this->vp_file);
  }
}  
