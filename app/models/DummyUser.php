<?php

namespace app\models;

use Yii;

class DummyUser implements \yii\web\IdentityInterface
{

    public $id = 1;
    public $username = 'dummyuser';    


    public static function findIdentity($id) {
        return new DummyUser();
    }

    public static function findIdentityByAccessToken( $token, $type = null ) {
        return null;
    }

    public function getAuthKey() {
        return null;
    }

    public function getId() {
        return $this->id;
    }

    public function validateAuthKey ( $authKey ) {
        return false;
    }
}
