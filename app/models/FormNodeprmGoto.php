<?php

namespace app\models;

use Yii;


class FormNodeprmGoto extends FormNodeprm {
	public $exten;
	public $context;
	

	public function exprAttributes() {
                return ['exten'];
	}

	public function rules() {
		return [
                        [['exten'], 'match', 'pattern' => '/^[\w_-]+$/'],
                        [['context'], 'match', 'pattern' => '/^[\w_-]+$/']
		];
	}


	public function attributeLabels() {
	        return [
	        	'exten' => Yii::t('app', 'Extension'),
	        	'context' => Yii::t('app', 'Context')
                ];
        }

        public function genName() {
        	if (!$this->context || !$this->exten)
        		return FormNodeprm::NAME_UNKNOWN;

		return self::normalizeName($this->context) . '@' . self::normalizeName($this->exten);
	}
}
