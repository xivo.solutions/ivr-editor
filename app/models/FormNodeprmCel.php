<?php

namespace app\models;

class FormNodeprmCel extends FormNodeprm {
  public $name;
  public $msg;

  const NAME_PATTERN = '/^[\w_-]+$/';
  const MSG_PATTERN = '/^[[:print:]]*$/';


  public function exprAttributes() {
    return ['name', 'msg'];
  }   

  public function runtimeRules() {
    return [
      [['name'], 'required'],
      [['name'], 'match', 'pattern' => self::NAME_PATTERN],
      [['msg'], 'match', 'pattern' => self::MSG_PATTERN],
    ];
  }

  public function attributeLabels() {
    return [
      'name' => \Yii::t('app', 'Name'),
      'msg' => \Yii::t('app', 'Mesasage'),
    ];
  }

  public function genName() {
    return self::normalizeName($this->name);
  }

}  
