<?php

namespace app\models;

use Yii;
use yii\base\Model;

use app\models\Nodeprm;


abstract class PrmForm extends Model {

  abstract public static function nameAttribute();
  abstract public static function valueAttribute();
  abstract public static function selectAttribute(); 
  abstract public static function recordClass();
  
  
  public function save($sel) {
    if (!$this->validate()) {
      return false;
    }

    $list = $this->toList($sel);

    $class = $this->recordClass();
    $class::deleteAll([$this->selectAttribute() => $sel]);

    $ret = true;
    foreach ($list as $prm) {
      $ret = $prm->save() && $ret;
    }
    
    return $ret;
  }
  
  
  public static function loadForm($fclass, $sel) {
    if (!is_string($fclass) || !is_subclass_of($fclass, "app\\models\\PrmForm")) {
      return null;
    }

    $rclass = $fclass::recordClass();
    $list = $rclass::findAll([$fclass::selectAttribute() => $sel]);
    return self::fromList($fclass, $list);
  }
  

  protected static function fromList($fclass, $list) {
    $form = new $fclass;
    
    foreach($list as $prm) {
      if (in_array($prm->{$fclass::nameAttribute()}, array_keys($form->attributes)))
        $form->{$prm->{$fclass::nameAttribute()}} = $prm->{$fclass::valueAttribute()};
    }
    
    return $form;
  }


  protected function toList($sel) {
    $list = array();
    $rclass = $this->recordClass();
    
    foreach ($this->attributes as $key => $val) {
      $prm = new $rclass;
      $prm->{$this->nameAttribute()} = $key;
      $prm->{$this->valueAttribute()} = $val === null ? '' : $val;
      $prm->{$this->selectAttribute()} = $sel;
      $list[] = $prm;
    } 
    return $list;
  }
}
