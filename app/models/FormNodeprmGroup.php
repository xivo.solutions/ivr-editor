<?php

namespace app\models;

use Yii;


class FormNodeprmGroup extends FormNodeprm {
	public $grpname;

	public function exprAttributes() {
                return [];
	}

	public function rules() {
		return [
                        [['grpname'], 'match', 'pattern' => '/^[\w_-]+$/'],
		];
	}


	public function attributeLabels() {
	        return [
	        	'grpname' => Yii::t('app', 'Group name'),
                ];
        }

        public function genName() {
        	return self::normalizeName($this->grpname, false);
	}

}
