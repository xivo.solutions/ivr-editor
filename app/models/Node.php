<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "node".
 *
 * @property int $id
 * @property int $id_flowchart
 * @property int $node_number
 * @property string $node_type
 * @property string $name
 *
 * @property Flowchart $flowchart
 * @property Nodecon[] $nodecons
 * @property Nodecon $nodecon
 * @property Nodepos $nodepos
 * @property Nodeprm[] $nodeprms
 */
class Node extends \yii\db\ActiveRecord
{
    
    const NODE_TYPE_START = 'start';
    const NODE_TYPE_ONHANGUP = 'onhangup';
    const NODE_TYPE_ONERROR = 'onerror';

    public static function tableName()
    {
        return 'ivr.ivredit_node';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_flowchart', 'node_number', 'node_type' ], 'required'],
            [['name'], 'required', 'strict' => true ],
            [['id_flowchart', 'node_number'], 'default', 'value' => null],
            [['id_flowchart', 'node_number'], 'integer'],
            [['node_type'], 'string', 'max' => 20],
            [['name'], 'string', 'max' => 50],
            [['id_flowchart', 'node_number'], 'unique', 'targetAttribute' => ['id_flowchart', 'node_number']],
            [['id_flowchart'], 'exist', 'skipOnError' => true, 'targetClass' => Flowchart::className(), 'targetAttribute' => ['id_flowchart' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
        ];
    }

    public static function uniqueNodeTypes() {
        return [ self::NODE_TYPE_START, self::NODE_TYPE_ONHANGUP, self::NODE_TYPE_ONERROR ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlowchart()
    {
        return $this->hasOne(Flowchart::className(), ['id' => 'id_flowchart']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNodecons()
    {
        return $this->hasMany(Nodecon::className(), ['id_flowchart' => 'id_flowchart', 'src' => 'node_number']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNodecon()
    {
        return $this->hasOne(Nodecon::className(), ['id_flowchart' => 'id_flowchart', 'dst' => 'node_number']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNodepos()
    {
        return $this->hasOne(Nodepos::className(), ['id_node' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNodeprms()
    {
        return $this->hasMany(Nodeprm::className(), ['id_node' => 'id']);
    }

    public function realName() {
        if ($this->name != '')
            return $this->name;
        
        return $this->automaticName();
            

    } 

    public function automaticName() {
        $fclass = FormNodeprm::formClass($this->node_type);
        $form = PrmForm::loadForm($fclass, $this->id);
        return $form->automaticName();
    }




}
