<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "voiceprompt".
 *
 * @property int $id
 * @property string $name
 * @property string $msg
 */
class Linefeatures extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'linefeatures';
    }

    public function getUserLine()
    {
        return $this->hasMany(UserLine::className(), [ 'line_id' => 'id' ]);
    }

    public function getUsers()
    {
        return $this->hasMany(Userfeatures::className(), ['id' => 'user_id'])
                ->via('userLine');
    }
}
