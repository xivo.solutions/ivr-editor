<?php

namespace app\models;

use app\components\ListValidator;

class FormNodeprmHttp extends FormNodeprm {
  public $url;
  public $method;
  public $variables;
  
  private $_methods = [ 'GET', 'POST' ];


  const SUBP_HOST = '([\w_-]+(\.[\w_-]+)*)';
  const SUBP_PORT = '(\d+)';
  const SUBP_PATH_ELEMENT = '([\w\d%+_.-]+)';

  const PATTERN_URL  = '#^https?:' . self::SUBP_PORT . '?//' . self::SUBP_HOST . '/(' . self::SUBP_PATH_ELEMENT . '/)*' . self::SUBP_PATH_ELEMENT . '?$#'; 
  
//  const SUBP_HOST = '([\w_-]+(\.[\w_-]+)*)';
//  const SUBP_PORT = '(:\d+)';
//  const SUBP_PATH_ELEMENT = '([\w\d+_.]+)';
  
//  const PATTERN_URL  = '#^https?' . self::SUBP_PORT . '?//' . self::SUBP_HOST . '/(' . self::SUBP_PATH_ELEMENT . '/)*' . self::SUBP_PATH_ELEMENT . '?#';
  
  public function getMethods() {
    return $this->_methods;
  }
  
  
  public function rules() {
    return [
      [['url'], 'match', 'pattern' => self::PATTERN_URL],
      [['variables'], 'app\components\ListValidator', 'delimiter' => '/\s+/' ,'rules' => [
        ['match', 'pattern' => FormNodeprm::VAR_PATTERN]
      ]],
      [['method'], 'in', 'range' => $this->getMethods()],
    ];
  }

  public function attributeLabels() {
    return [
      'url' => \Yii::t('app', 'URL'),
      'method' => \Yii::t('app', 'Method'),
      'variables' => \Yii::t('app', 'Variables'),
    ];
  }

  public function genName() {
    return self::normalizeName($this->url);
  }
}  
