<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "voiceprompt".
 *
 * @property int $id
 * @property string $name
 * @property string $msg
 */
class Userfeatures extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userfeatures';
    }

    public function getUserLine()
    {
        return $this->hasMany(UserLine::className(), [ 'user_id' => 'id' ]);
    }

    public function getLines() {
        return $this->hasMany(Linefeatures::className(), ['id' => 'line_id'])
            ->via('userLine');
    } 
}
