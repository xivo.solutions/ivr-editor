<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "voiceprompt".
 *
 * @property int $id
 * @property int $id_account
 * @property string $name
 * @property string $source
 * @property string $msg
 * @property string $lang
 * @property string $speed
 *
 * @property Account $account
 */
class Voiceprompt extends \yii\db\ActiveRecord
{

    const SRC_UPLOAD = 'upload';
    const SRC_TTS = 'tts';
    const SRC_RECORD = 'record';

    const SC_UPLOAD = 'upload';
    const SC_TTS = 'tts';
    const SC_RECORD = 'record';


    public $file;
    public $keep;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ivr.ivredit_voiceprompt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name',], 'required'],
            [['file'], 'required', 'on' => [self::SC_UPLOAD]],
            [['lang'], 'required', 'on' => [self::SC_TTS]],
            [['msg'], 'string'],
            [['speed'], 'number', 'on' => [self::SC_TTS]],
            [['name'], 'string', 'max' => 255],
            [['lang'], 'string', 'max' => 2, 'on' => [self::SC_TTS]],
            [['file'], 'file', 'on' => [self::SC_UPLOAD]]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'source' => Yii::t('app', 'Source'),
            'msg' => Yii::t('app', 'Message'),
            'lang' => Yii::t('app', 'Language'),
            'speed' => Yii::t('app', 'Speed'),
            'file' => Yii::t('app', 'File')
        ];
    }


    public function languages() {
        return array(
            'en' => 'English',
            'cs' => 'Czech'
        );
    }

    private function hasName() {
        return isset($this->name) || $this->name != '';
    }

    public function validate($attributeNames = NULL, $clearErrors = true) {
        if(!$this->hasName()) {
            $this->name = pathinfo($this->file->name, PATHINFO_FILENAME);
        }
        return parent::validate($attributeNames, $clearErrors);
    }
}
