<?php

namespace app\models;

use app\models\Voicemail;

class FormNodeprmVoicemail extends FormNodeprm {
  public $uniqueid;

  public function runtimeRules() {
    return [
      [['uniqueid'], 'exist', 'targetClass' => Voicemail::class]
    ];
  }

  public function attributeLabels() {
    return [
	'uniqueid' => \Yii::t('app', 'Voicemail ID'),
    ];
  }

  public function viewExtraData() {
    $vm = Voicemail::findOne($this->uniqueid);
    return $vm ? $vm : null;
  }

  public function genName() {
    $vm = Voicemail::findOne($this->uniqueid);
    if (!$vm)
      return '';
    return self::normalizeName($vm->fullname, false);
  }

}

