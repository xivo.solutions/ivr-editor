<?php

namespace app\models;

class FormNodeprmRecstart extends FormNodeprm {
	public $file;
	public $bridged;
	public $volin;
	public $volout;


	public function exprAttributes() {
                return ['file','bridged', 'volin', 'volout'];
	}

	public function runtimeRules() {
		return [
			[['file'], 'required'],
			[['file'], 'match', 'pattern' => '/^[\w._-]+$/'],
			[['bridged'], 'integer', 'min' => 0, 'max' => 1],
			[['volin'], 'integer', 'min' => -4, 'max' => 4],
			[['volout'], 'integer', 'min' => -4, 'max' => 4],
		];
	}


	public function attributeLabels() {
	        return [
	                'file' => \Yii::t('app', 'File'),
	                'bridged' => \Yii::t('app', 'Bridged only'),
	                'volin' => \Yii::t('app', 'Incoming audio volume adjustment'),
	                'volout' => \Yii::t('app', 'Outgoing audio volume adjustment'),
	        ];
        }
	          
        public function genName() {
        	self::normalizeName($this->file);
	}
}
