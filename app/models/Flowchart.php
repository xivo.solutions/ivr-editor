<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flowchart".
 *
 * @property int $id
 * @property string $name
 *
 * @property Node[] $nodes
 */
class Flowchart extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ivr.ivredit_flowchart';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique', 'message' => Yii::t('app', 'Name already exists')],
            [['name'], 'string', 'max' => 50],
        ];
    }


    public function used() {
        return Dialaction::find()
            ->where([
                'event' => 'answer',
                'category' => 'incall',
                'action' => 'ivr',
                'actionarg1' => $this->id
            ])->exists();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNodes()
    {
        return $this->hasMany(Node::className(), ['id_flowchart' => 'id']);
    }

    public function getNodecons() {
        return $this->hasMany(Nodecon::className(), ['id_flowchart' => 'id']);
    }
}
