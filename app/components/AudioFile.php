<?php

namespace app\components;

class AudioFile {
    private static $extlist = array("mp3", "wav", "gsm", "al", "ul", "g729");
    private static $sox_ast_wav_fmt = "-r8k -b16 -t wav";


    public static function checkName($name) {
        if ($name == ".." or $name == "." or strpos($name, "/") !== false)
            return false;
            
        return true;
    }


    public static function findFileInDir($dir, $name, $hasext = false) {
	if (!self::checkName($name))
	    return false;
	
	if ($hasext) {
	    $f = $dir . "/" . $name;
	    if (file_exists($f))
                return $name;
	} else {
	    foreach (self::$extlist as $ext) {
		$f = $dir . "/" . $name . "." . $ext;
		if (file_exists($f))
                    return $name . "." . $ext;
            }
        }
	
	return false;
    }


    public static function findPathInDir($dir, $name, $addext = true) {
        $fname = self::findFileInDir($dir, $name, $addext);
        if (!$fname)
            return false;
        
        return $dir . "/" . $fname;
    }
            
    public static function isMP3($file) {
	$res = finfo_open();
	$mime = finfo_file($res, $file);
	
	finfo_close($res);

	return preg_match("/MPEG|with ID3/", $mime);
    }

    
    public static function convertToWav($file) {
        $tmp1 = tempnam(sys_get_temp_dir(), "tmp-convert-");
        $tmp = $tmp1 . ".wav";

        if (self::isMP3($file)) {
            exec("mpg123  -w $tmp -r 8000 -m  $file", $arr, $ret);
	} else {
            exec("/usr/bin/sox $file -r 8k -b16 $tmp", $arr, $ret);
        }

	if ($ret != 0) {
            return false;
        }
            
	return $tmp;
    }


    public static function changeTempo($file, $factor) {
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $tmp1 = tempnam(sys_get_temp_dir(), "tmp-tempo-");
        $tmp = $tmp1 . "." . $ext;
        
        exec("sox $file $tmp tempo -s $factor", $arr, $ret);
        
        return $tmp;
    }


    protected static function getTTS($lang, $words) {
        $headers = array(
            'User-Agent: Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.5) Gecko/20091109 Ubuntu/9.10 (karmic) Firefox/3.5.5',
            'Accept-Language: en-us,en;q=0.5',
            'Accept-Encoding: gzip,deflate',
            'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7',
            'Keep-Alive: 300',
            'Connection: keep-alive'
        );


        $url = sprintf("https://translate.google.com/translate_tts?tl=%s&q=%s", $lang, $words);
        $ch = curl_init($url);
        
        $tmp = tempnam("/tmp", "cc-tts-");
        $fp = fopen($tmp, "w");
        
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
        return $tmp;
    } 


    public static function tts($text, $lang) {
        $words = urlencode($text);

        $tmp = self::getTTS($lang, $words);

        if (!self::isMP3($tmp)) {
            unlink($tmp);
            return false; 
        }
        
        $f = self::convertToWav($tmp);
        unlink($tmp);
       
        return $f;
    }


    public function multi_tts($text, $lang) {
        $ret = null;
        
        $rows = preg_split("/\n/", $text);
        $files = array();
        
        foreach($rows as $r) {
            if (!($f = self::tts($r, $lang)))
                goto finish;
            
            array_push($files, $f);
        }
        
        if (count($files) == 1)
            return $files[0];
        
        $ret = $files[0];
        for($i = 1; $i < count($files); $i++) {
            $ret = self::concat_files($ret, $files[$i]);
            if (!$ret)
                break;
            
        }
        
    finish:
        foreach($files as $f)
            unlink($f);
        
        return $ret;
    }


    public function concat_files($f1, $f2) {
        $tmp1 = tempnam(sys_get_temp_dir(), "tmp-convert-");
        $tmp = $tmp1 . ".wav";
        
        exec("sox $f1 $f2 $tmp 2>/dev/null", $arr, $ret);

        if ($ret != 0)
            return null;
        else 
            return $tmp;
    }



    public static function mkSilence($len) {
        $len += 0;
        if ($len <= 0.1)
            $len = 0.1;

        $tmp = tempnam(sys_get_temp_dir(), "tmp-silence-");
        $cmd = sprintf("sox -n %s %s trim 0 %f", self::$sox_ast_wav_fmt, $tmp, $len);
        exec(escapeshellcmd($cmd), $arr, $ret);
        if ($ret != 0)
            return false;
        
        return $tmp;
    }
}
