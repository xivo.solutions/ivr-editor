<?php

namespace app\components;

use Yii;

use yii\base\Exception;
use yii\helpers\ArrayHelper;

use app\components\startNode;
use app\components\playbackNode;
use app\components\menuNode;
use app\components\hangupNode;
use app\components\assignNode;
use app\components\readNode;
use app\components\saynumberNode;
use app\components\conditionNode;
use app\components\phoneNode;
use app\components\dataNode;
use app\components\recstartNode;
use app\components\recstopNode;
use app\components\waitNode;
use app\components\onerrorNode;
use app\components\onhangupNode;
use app\components\celNode;
use app\components\queueNode;
use app\components\gotoNode;
use app\components\groupNode;
use app\components\userNode;
use app\components\voicemailNode;

use app\models\PrmForm;
use app\models\FormNodeprm;
use app\models\Node;

abstract class DialplanNode {

  const OPT_ERROR = '_error';

  protected $form;
  
  private static $_node_types = [
    Node::NODE_TYPE_START,
    Node::NODE_TYPE_ONHANGUP,
    Node::NODE_TYPE_ONERROR,
    "playback", 
    "menu", 
    "hangup",
    "assign", 
    "read", 
    "saynumber", 
    "condition", 
    "phone",
    "data",
    "recstart",
    "recstop",
    "wait",
    "cel",
    "queue",
    "http",
    "goto",
    "group",
    "user",
    "voicemail",
  ];
  
  public function __construct($id) {
    $this->params = array();

    $this->id = $id;    
    $c = get_called_class();
    $formclass = FormNodeprm::formClass($c::getType());
    $this->form = PrmForm::loadForm($formclass, $id);
  }
  
  
  final public static function allNodes($sorted = false) {
    $a = self::$_node_types;
    
    if ($sorted)
      sort($a);
    
    return $a;
  }


  final public static function enabledNodes($sorted = false) {
    $nodes = self::allNodes($sorted);
    $dis = ArrayHelper::getValue(\Yii::$app->params, 'disabled_nodes');
    
    if (!$dis)
      return nodes;
    
    if (!is_array($dis))
      $dis = [$dis];
    
    foreach ($nodes as $idx => $val) {
      if (in_array($val, $dis)) {
        unset($nodes[$idx]);
      }
    }
    
    return $nodes;
  }


  public function enabled() {
    $nodes = self::enabledNodes();
    return in_array($this->getType(), $nodes);
  }
    
  
  public static function optList() {
    return [];
  }


  public abstract function run($agi, &$dp_status, &$user_variables);


  final public static function typeToClassName($type) {
    $c = "app\\components\\" . $type . "Node";
    if (class_exists($c))
      return $c;
      
    throw new Exception("Unknown node type " . $c);
  }

  
  public static function getLabel() {
    $c = get_called_class();
    return Yii::t('node', $c::getType());
  }

  function evalParams($user_variables) {
    $f = clone $this->form;
    if (!$f->evaluate($user_variables))
      return null;
      
    $f->scenario = 'runtime';
    return $f;
  }
  
  
  public function optValid($opt) {
    return in_array($opt, $this->optList(), 1);
  }


  public static function getType() {
    return substr(get_called_class(), 15, -4);
  }

  public function connectedOpts() {
    $node = Node::find()->where(['id' => $this->id])->with("nodecons")->one();
    $ret = [];
    foreach($node->nodecons as $con) {
      $ret[] = $con->src_opt;
    }
    return $ret;
  }
}
