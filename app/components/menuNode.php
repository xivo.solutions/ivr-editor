<?php

namespace app\components;

class menuNode extends DialplanNode {
  
  private const RUN_STATE_START = 0;
  private const RUN_STATE_HAVEDIGIT = 1;
  private const RUN_STATE_NOCHOICE = 2;
  private const RUN_STATE_INVALID = 3;
  

  public static function optList() {
    return array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '#');
  }

  
  public function run($agi, &$dp_status, &$user_variables) {
    if (!($f = $this->evalParams($user_variables)))
      return self::OPT_ERROR;
    if (!$f->validate())
      return self::OPT_ERROR;
    $timeout = $f->timeout * 1000;
    $maxcount = $f->repeat + 0;
    
    $count = -1;

    $vp_menu = VoicePromptFile::getPlayPath('vp', $f->vp_menu);
    $vp_nochoice = VoicePromptFile::getPlayPath('vp', $f->vp_nochoice);
    $vp_invalid = VoicePromptFile::getPlayPath('vp', $f->vp_invalid);    
    $valid_options = $this->connectedOpts();
    
    $state = self::RUN_STATE_START;
    while ($count < $maxcount) { 
      if (($state == self::RUN_STATE_NOCHOICE || $state == self::RUN_STATE_INVALID) && $vp_nochoice) {
        $digit = $this->receive_digit($agi, $vp_nochoice, 0);
        if ($digit > 0)
          $state = self::RUN_STATE_HAVEDIGIT;
      } 
      
      if ($state != self::RUN_STATE_HAVEDIGIT) {
        $digit = $this->receive_digit($agi, $vp_menu, $timeout);
      }
      
      $agi->debug("menuNode received code: {$digit}");
      $count++;
      
      if ($digit < 0) {
        $agi->debug("menuNode GET OPTION errror");
        return self::OPT_ERROR;
      }
      
      if ($digit == 0) {			// no digit
        $agi->debug("menuNode no digit received");
        $state = self::RUN_STATE_NOCHOICE;
        continue;
      }
      
      if (!in_array(chr($digit), $valid_options)) {
        $agi->debug("menuNode invalid digit received");
        $state = self::RUN_STATE_INVALID;
        continue;
      }
      
      break;
    }
    
    $agi->debug("menuNode received code: {$digit}");
    if ($state == self::RUN_STATE_NOCHOICE || $state == self::RUN_STATE_INVALID) {
      if ($vp_nochoice) {
          $digit = $this->receive_digit($agi, $vp_invalid, 0);
          return 'f';
      }
    } else {
      return chr($digit);
    }
  }

  private function receive_digit($agi, $vp, $timeout) {
    return $agi->cmd_get_option($vp, "0123456789*#", $timeout);
  }
}

?>
