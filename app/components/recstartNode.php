<?php

namespace app\components;

class recstartNode extends DialplanNode {

	public static function optList() {
		return array('next');
	}


	public function run($agi, &$dp_status, &$user_variables) {
	        if (!($f = $this->evalParams($user_variables)))
	                return self::OPT_ERROR;
    
                if (!$f->validate()) {
                        return self::OPT_ERROR;
                }
	
                if (isset($dp_status[Dialplan::STATUS_MONITOR]) && $dp_status[Dialplan::STATUS_MONITOR])
                        return 'next';
                
                
                $path = Yii::app()->params['clientMonitorDir'] . "/" . $f->file . ".wav";

                if (!$path)
                        $agi->debug("path empty");
	
                $opt = '';
                
                if ($f->bridged)
                        $opt .= 'b';
                
                if ($f->volin)
                        $opt .= "(v({$f->volin})";
                
                if ($f->volout)
                        $opt .= "(v({$f->volout})";
                
	
	
                $agi->exec_mixmonitor($path, $opt);
		$dp_status[Dialplan::STATUS_MONITOR] = 1;
		return 'next';
	}
}

?>
