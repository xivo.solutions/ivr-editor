<?php

namespace app\components;

class readNode extends DialplanNode {

	public static function optList() {
		return array('ok', 'fail');
	}


	public function run($agi, &$dp_status, &$user_variables) {
	        if (!($f = $this->evalParams($user_variables)))
	                return self::OPT_ERROR;
    
                if (!$f->validate()) {
                        return self::OPT_ERROR;
                }
	
		$playprm = VoicePromptFile::getPlayPath('vp', $f->vp_file);
		$invalidprm = $f->vp_invalid ? VoicePromptFile::getPlayPath('vp', $f->vp_invalid) : '';
		

		$f->min_digits += 0;
		$f->max_digits += 0;
		$f->min += 0;
		$f->max += 0;
		$f->repeat += 0;
		
		if ($f->beep)
			$playprm .= "&beep";
		
		$ok = 0;
		$count = -1;
		while ($count < $f->repeat) { 
			$count++;
			$agi->cmd_set_variable("CLIENT_READ", "");
			$agi->exec_read("CLIENT_READ", $playprm, $f->max_digits, 's');
			$result = $agi->cmd_get_variable("CLIENT_READ");
			
			if (strlen($result) < $f->min_digits)
				  goto cont;
			
			if ($f->max_digits && strlen($result) > $f->max_digits)
				goto cont;

			if ($f->numeric) {
				if (!preg_match('/^\d+$/', $result))
					goto cont;
				
				$num = $result + 0;
				if ($f->min !== false && $num < $f->min)
					goto cont;
				
				if ($f->max !== false && $num > $f->max)
					goto cont;
			}
				
			
			$ok = 1;
			break;
			
			cont:
			if ($invalidprm)
				$agi->exec_playback($invalidprm);
                        
                        continue;
                }
		
		if ($ok) {
			$user_variables[substr($f->var,1)] = $result;
			return 'ok';
		} else {
			return 'fail';
		}
	}
}

?>
