<?php

namespace app\components;

class dataNode extends DialplanNode {

  public static function optList() {
    return array('next');
  }
  
  public function run($agi, &$dp_status, &$user_variables) {
    if (!($f = $this->evalParams($user_variables)))
      return self::OPT_ERROR;
    
    if (!$f->validate())
      return self::OPT_ERROR;
      
    $url = settings('dataURL');
    if (!$url)
      return self::OPT_ERROR;
    
    if ($f->command)
      $url .= "?cmd=" . $f->command; 
    
    $h = curl_init($url);
    curl_setopt($h, CURLOPT_POST, true);
    curl_setopt($h, CURLOPT_HEADER, false);
    
    $a = preg_split('/\s+/', $f->variables);
    $data = array();
    foreach ($a as $var) {
      $vname = substr($var, 1);
      $data[$vname] = $user_variables[$vname];
    }
    
    $q = http_build_query($data);
    
//    curl_setopt($h, CURLOPT_SAFE_UPLOAD, true);
    curl_setopt($h, CURLOPT_POSTFIELDS, $q);
    curl_setopt($h, CURLOPT_RETURNTRANSFER, true);

    $ret = curl_exec($h);
    curl_close($h);

    $recvdata = $this->parseData($ret, $agi);
    if ($recvdata === false) {
      $agi->debug("parse failed");
      return self::OPT_ERROR;
    }
    
    foreach ($recvdata as $var => $v)
      $user_variables[$var] = $v;
      
    return 'next';
  }


  protected function parseData($datastr, $agi) {
    $lines = preg_split("/\n/", $datastr);
    $ret = array();
    foreach($lines as $d) {
      if (trim($d) == '')
        continue;
        
      $pos = strpos($d, ' ');
      if ($pos === false) {
        $var = $d;
        $value = '';
      } else {
        $var = substr($d, 0, $pos);
        $value = substr($d, $pos+1);
      }
      
      if (!preg_match(FormNodePrm::VAR_PATTERN, $var)) {
        $agi->debug("var: " . $var);
        return false;
      }
      
      $ret[substr($var,1)] = $value;
    }
    
    return $ret;
  }

}

?>
