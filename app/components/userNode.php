<?php

namespace app\components;

use app\models\Userfeatures;

class userNode extends DialplanNode {

  public static function optList() {
    return [];
  }


  public function run($agi, &$dp_status, &$user_variables) {
    if (!($f = $this->evalParams($user_variables)))
      return self::OPT_ERROR;

    if (!$f->validate())
      return self::OPT_ERROR;

    $uf = Userfeatures::find()->with('lines')->where(['id' => $f->id])->one();

    if (!$uf || empty($uf->lines) || empty($uf->lines[0]->context) || empty($uf->lines[0]->number))
      return self::OPT_ERROR;

    $agi->exec_goto($uf->lines[0]->context, $uf->lines[0]->number, 1);
    return '';
  }
}

?>
