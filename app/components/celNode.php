<?php

namespace app\components;

class celNode extends DialplanNode {

  public static function optList() {
    return array('next');
  }
  
  public function run($agi, &$dp_status, &$user_variables) {
    if (!($f = $this->evalParams($user_variables))) {
      return self::OPT_ERROR;
      
    }

    if (!$f->validate())
      return self::OPT_ERROR;
    
    $agi->exec_celgenuserevent($f->name, $f->msg);
    return 'next';
  }
}

?>
