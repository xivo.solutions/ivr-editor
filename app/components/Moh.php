<?php


namespace app\components;


class MoH {

    const BASEDIR = "/var/lib/asterisk/appmoh";
    const SLOTS = 10;
    const STOREDIR = "files";
    

    private static function classdir() {
        return self::BASEDIR . "/" . \Config::get("app.machine_name");
    }
    
    private static function storedir() {
        return self::classdir() . "/" . self::STOREDIR;
    }


    private static function defaultStorePath() {
        return self::storedir() . "/" . "default.wav";
    }
    
    
    private static function defaultMohPath() {
        return self::classdir() . "/" . "default.wav";
    }
    

    private static function mohFileInfo($fname) {
        if (!preg_match("/.wav\$/", $fname))
            return false;
            
        $pref = substr($fname, 0, 2);
        if ($pref == "xx")
            $slot = -1;
        else if (ctype_digit($pref[0]) and ctype_digit($pref[1]))
            $slot = $pref + 0;
        else
            return false;
            
        $name = substr($fname, 2, strlen($fname) - 6);
        if (strlen($name) == 0)
            return false;
            
        return array('name' => $name, 'slot' => $slot);
    } 
    
    
    private static function checkSlot($idx) {
        return is_integer($idx) and  $idx >= 0 and $idx < self::SLOTS;
    }


    private static function getFreeSlot() {
        $list = self::listActiveSorted();
        
        for ($i = 0; $i < count($list); $i++) {
            if ($i < $list[$i]['slot'])
                return $i;
        }
        
        if ($i < self::SLOTS) 
            return $i;
        
        return -1;
    }


    public static function findFileByName($name) {
        $fi = self::getNameInfo($name);
        if (!$fi)
            return false;
            
        return self::slotToStorePath($fi['name'], $fi['slot']);
    }
        
                
        
        
    private static function slotToStorePath($name, $idx) {
        $fname = self::slotToStoreFile($name, $idx);
        if (!$fname)
            return false;

        return self::storedir() . "/" . $fname;
    }
    
    
    private static function slotToStoreFile($name, $idx) {
        $idx += 0;
        if(!self::checkSlot($idx) && $idx != -1)
            return false;
            
        if (!AudioFile::checkName($name))
            return false;
            
        if ($idx == -1)
            return sprintf("xx%s.wav", $name);
        else
            return sprintf("%02d%s.wav", $idx, $name);
    }
    
    
    private static function slotToMohFile($idx) {
        $idx += 0;
        if(!self::checkSlot($idx))
            return false;
            
        return sprintf("%02d.wav", $idx);
    }
    
    
    private static function slotToMohPath($idx) {
        $idx += 0;
        if(!self::checkSlot($idx))
            return false;
            
        return sprintf("%s/%02d.wav", self::classdir(), $idx);
    }
    
    
    private static function findNameInList($name, $list) {
        for ($i = 0; $i < count($list); $i++) {
            if ($list[$i]['name'] == $name)
                return $i;
        }
        
        
        return false;
    }
        
    
            
    private static function clearSlot($idx) {
        $idx += 0;
        if (!self::checkSlot($idx))
            return false;
        
        $f = AudioFile::mkSilence(0.1);
        if (!$f)
            return false;
        
        $target = self::slotToMohPath($idx);
        
        exec("mv $f $target", $arr, $ret);
        $cret = chmod($target, 0644);

        return $ret == 0;
    }
    
    private static function setDefaultIfEmpty($list) {
        
        foreach($list as $fi) {
            if ($fi['slot'] >= 0)
                return;
        }
            
        $def = self::defaultStorePath();
        $target = self::defaultMohPath();
        
        unlink($target);
        link($def, $target);
    }
        
        
    private static function unsetDefault() { 
        $f = AudioFile::mkSilence(0.1);
        if (!$f)
            return false;
            
        $target = self::defaultMohPath();
        exec("mv $f $target", $arr, $ret);
        chmod($target, 0644);
        
        return $ret == 0;
    }
    
    
    private static function getNameInfo($name) {
        $list = self::listAll();
    
        foreach($list as $f) {
            if ($f['name'] == $name)
                return $f;
        }
        
        return false;
    }
    

    private static function cmp_fi_name($fi1, $fi2) {
        if ($fi1['name'] > $fi2['name'])
            return 1;
            
        return $fi1['name'] < $fi2['name'] ? -1 : 0;
    }


    public static function listAll() {
        $ret = array();
        $dh = opendir(self::storedir());
        while (($fname = readdir($dh)) !== false) {
            $fi = self::mohFileInfo($fname);
            if ($fi === false)
                continue;
            $ret[] = $fi;
        }
        
        closedir($dh);
        
        usort($ret, "self::cmp_fi_name");
        
        return $ret;
    }        


    public static function listActive() {
        $ret = array();
        $dh = opendir(self::storedir());
        while (($fname = readdir($dh)) !== false) {
            $fi = self::mohFileInfo($fname);
            if ($fi === false or $fi['slot'] == -1)
                continue;

            $ret[] = $fi;
        }
        
        closedir($dh);
        return $ret;
    }        
    
    
    
    private static function cmp_fi_slot($fi1, $fi2) {
        if ($fi1['slot'] > $fi2['slot'])
            return 1;
            
        return $fi1['slot'] < $fi2['slot'] ? -1 : 0;
    }
            
    
    
    public static function listActiveSorted() {
        $list = self::listActive();
        if ($list === false)
            return false;
            
        usort($list, "self::cmp_fi_slot");
        return $list;
    }
        
            
    public static function delete($name) {
        if (!AudioFile::checkName($name))
            return false;
    
        
        $list = self::listAll();
        if ($list === false)
            return false;


        $idx = self::findNameInList($name, $list);
        if ($idx === false)
            return false;
                 
        
        $storefile = self::slotToStorePath($list[$idx]['name'], $list[$idx]['slot']);
        unlink($storefile);
        
        if ($list[$idx]['slot'] >= 0)
            self::clearSlot($list[$idx]['slot']);
        
        unset($list[$idx]);
        self::setDefaultIfEmpty($list);
        
        return true;
    }
        
    
    public static function save($file, $name) {
        if (!AudioFile::checkName($name))
            return false;
        
        $wavfile = AudioFile::convertToWav($file);
        unlink($file);
        
        if (!$wavfile)
            return false;
            
        $fi = self::getNameInfo($name);
        
        if (!$fi)
            $slot = -1;
        else
            $slot = $fi['slot'];
        
        $storefile = self::slotToStorePath($name, $slot);
        exec("mv $wavfile $storefile ", $arr, $ret);
        
        if ($slot >= 0) {
            $mohfile = self::slotToMohpath($slot); 
            copy($storefile, $mohfile);
        }
        
        return true;
    }
        
            
    public static function activate($name) {
        $fi = self::getNameInfo($name);
        if (!$fi)
            return false;
            
        if ($fi['slot'] >= 0)
            return true;
        
        $slot = self::getFreeSlot();
        if ($slot < 0)
            return false;
        
        $oldfile = self::slotToStorePath($name, -1);
        $newfile = self::slotToStorePath($name, $slot);
        $mohfile = self::slotToMohPath($slot);
        
        rename($oldfile, $newfile);
        copy($newfile, $mohfile);

        self::UnsetDefault();
        
        return true;
    }
    
    
    public static function deactivate($name) {
        $list = self::listAll();
        if ($list === false)
            return false;
        
        

        
        $idx = self::findNameInList($name, $list);
        if ($idx === false)
            return false;
        
        $fi = $list[$idx];
        
        $slot = $fi['slot'];
        if ($slot < 0)
            return true;
        
        $oldfile = self::slotToStorePath($name, $slot);
        $newfile = self::slotToStorePath($name, -1);
     
        rename($oldfile, $newfile);
        self::clearSlot($slot);
        
        $list[$idx]['slot'] = -1;
        self::setDefaultIfEmpty($list);
        
        
        return true;
    }
    
        
    public static function orderActive($names) {
        if (!is_array($names))
            return false;
       
        $list = self::listActive();
       
        $sorted = array();
        foreach($names as $n) {
            for ($i = 0; $i < count($list); $i++) {
                if (isset($list[$i]) and $n == $list[$i]['name']) {
                    $sorted[] = $list[$i];
                    unset($list[$i]);
                    break;
                }
            }
        }
       
        foreach($list as $fi)
            $sorted[] = $fi;
       
        
        // work in 2 steps to avoid file overwriting
        for($i = 0; $i < count($sorted); $i++) {
            $oldfile = self::slotToStorePath($sorted[$i]['name'], $sorted[$i]['slot']);
            $tmpfile = self::slotToStorePath($sorted[$i]['name'], -1);
            
            rename($oldfile, $tmpfile);
        }
        
        
        for($i = 0; $i < count($sorted); $i++) {
            $tmpfile = self::slotToStorePath($sorted[$i]['name'], -1);
            $newfile = self::slotToStorePath($sorted[$i]['name'], $i);
            $mohfile = self::slotToMohPath($i);

            rename($tmpfile, $newfile);
            unlink($mohfile);
            link($newfile, $mohfile);
        }
        
        for ($i = count($sorted); $i < self::SLOTS; $i++)
            self::clearSlot($i);
        
        return true;
    }
}

?>
