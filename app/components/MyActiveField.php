<?php

namespace app\components;

use yii\widgets\ActiveField;


class MyActiveField extends ActiveField {
    
    public $switchElement = '<i class="fa fa-refresh" style="cursor: pointer" onclick="switch_input(this)"></i>';
    
    const SWITCH_ON = 'switch-on';
    const SWITCH_OFF = 'switch-off';
    
    public function vpInput($selData, $switch_state, $selOptions = [], $textOptions = []) {
        $html = '';
        
        $swclass = 'switch_' . $this->attribute;
        $label = $this->model->attributeLabels()[$this->attribute] . ' ' . $this->switchElement;
        
        $this->label($label, ['swclass' => $swclass]);
        
        $intclass = [$swclass, $switch_state ?  self::SWITCH_ON :  self::SWITCH_OFF];
        
        
        if (!isset($selOptions['class']))
            $selOptions['class'] = [];
        
        if (!isset($textOptions['class']))
            $textOptions['class'] = [];
     
        $selOptions['class'][] = $swclass;
        $selOptions['class'][] = $switch_state ?  self::SWITCH_ON :  self::SWITCH_OFF;
        
        $textOptions['class'][] = $swclass;
        $textOptions['class'][] = !$switch_state ?  self::SWITCH_ON :  self::SWITCH_OFF;

        $html .= $this->dropDownList($selData, $selOptions);
        $html .= $this->label(false)->textInput($textOptions);
        
        return $html; 
    }
}