<?php

namespace app\components;

class assignNode extends DialplanNode {

  public static function optList() {
    return array('next');
  }
  
  public function run($agi, &$dp_status, &$user_variables) {
    if (!($f = $this->evalParams($user_variables)))
      return self::OPT_ERROR;
    
    if (!$f->validate())
      return self::OPT_ERROR;
      
    $var = substr($f->var, 1);
    $user_variables[$var] = $f->expr;
    return 'next';
  }
}

?>
