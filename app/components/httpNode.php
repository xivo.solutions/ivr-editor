<?php

namespace app\components;

class httpNode extends DialplanNode {

  public static function optList() {
    return array('next');
  }
  
  public function run($agi, &$dp_status, &$user_variables) {
    if (!($f = $this->evalParams($user_variables)))
      return self::OPT_ERROR;
    
    if (!$f->validate())
      return self::OPT_ERROR;
      
    $url = $f->url;
    if (!$url)
      return self::OPT_ERROR;
    
    
    $a = preg_split('/\s+/', $f->variables);
    $data = array();
    foreach ($a as $var) {
      $vname = substr($var, 1);
      $data[$vname] = $user_variables[$vname];
    }
    
    
    $h = curl_init();
    if ($f->method == "POST") {
      
      curl_setopt($h, CURLOPT_POST, true);
      curl_setopt($h, CURLOPT_POSTFIELDS, $q);
    } else {
      $url = $this->append_query_string($url, $data);
    }  
      
    curl_setopt($h, CURLOPT_URL, $url);
    
    curl_setopt($h, CURLOPT_HEADER, false);
    
//    curl_setopt($h, CURLOPT_SAFE_UPLOAD, true);
    curl_setopt($h, CURLOPT_RETURNTRANSFER, true);

    $ret = curl_exec($h);
    curl_close($h);

    if ($ret === false) {
      $agi->debug("http query failed");
      return self::OPT_ERROR;
    }
    
    return 'next';
  }

  
  protected function append_query_string($url, $data) {
    $qs = strpos($url, '?') === true ? '' : '?';
    foreach ($data as $name => $value) {
      if ($qs != '?')
        $qs .= '&';
      
      $qs .= $name . "=" . urlencode($value);
    }
    
    return $url . $qs;
  }

}

?>
