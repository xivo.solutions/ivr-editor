<?php

namespace app\components;

use yii\base\Exception;

class Expression {
  
  private $expr;
  
  private $ignored_tokens = array ( T_COMMENT, T_DOC_COMMENT, T_WHITESPACE);
  private $allowed_tokens = array ( 
    T_BOOLEAN_AND,
    T_BOOLEAN_OR,
    T_BOOL_CAST,
    T_CONSTANT_ENCAPSED_STRING,
    T_DNUMBER,
    T_DOUBLE_CAST,
    T_EMPTY,
    T_ENCAPSED_AND_WHITESPACE,
    T_INT_CAST,
    T_ISSET,
    T_IS_EQUAL,
    T_IS_GREATER_OR_EQUAL,
    T_IS_IDENTICAL,
    T_IS_NOT_EQUAL,
    T_IS_NOT_IDENTICAL,
    T_IS_SMALLER_OR_EQUAL,
    T_LNUMBER,
    T_LOGICAL_AND,
    T_LOGICAL_OR,
    T_LOGICAL_XOR,
//    T_POW,
    T_SL,
    T_SR,
    T_STRING,
    T_STRING_CAST,
    T_UNSET_CAST,
    T_VARIABLE,
    '+',
    '-',
    '*',
    '/',
    '%',
    '>',
    '<',
    '|',
    '^',
    '~',
    '&',
    '!',
    '?',
    ':',
    '(',
    ')',
    '.'
  );
    
  private $allowed_functions = array (
    "substr",
    "strpos",
    "stripos",
    "strcmp",
    "strcasecmp",
    "strlen",
    "strrev",
    "strtolower",
    "strtoupper",
    "time",
    "strftime"
  );
    
    
  
  
  
  public function __construct($expr = "''") {
    if (false === ($ret = $this->validate($expr, $err)))
      throw new Exception($err);
  
    $this->expr = $ret;
  }
  
  
  
  /* TODO !!! allowed functions */
  
  protected function validate($expr, &$err) {
    $err = "";
    
    if (!is_string($expr)) {
      $err = "Internal error, expression is not string";
      return false;
    }
    
    $tok_expr = "<?php " . $expr . ";";
    
    $code = escapeshellarg($tok_expr);
    $lint = `echo $code | php -l`;
    if (!preg_match("/^No syntax errors detected/", $lint)) {
      $err = $lint;      
      return false;
    }

    $tlist = token_get_all($tok_expr);
    
    $ret = "";
    array_shift($tlist);	// remove open tag
    array_pop($tlist);		// remove last ; 
    $varseen = '';
    foreach ($tlist as $t) {
      if (is_array($t)) {
        $tid = $t[0];
        $tcode = $t[1];
      } else
        $tid = $tcode = $t;

      if (in_array($tid, $this->ignored_tokens, true)) {
        $ret .= $tcode;
        continue;
      }

      if (!in_array($tid, $this->allowed_tokens, true)) {
        $err = sprintf("Prohibited token '%s' ('%s')", is_string($tid) ? $tid : token_name($tid), $tcode);
        return false;
      }
      
      if ($varseen && $tid == "(") {
        $err = "Dynamic function call not allowed '" . $varseen . "()'";
        return false;
      }
    
      if ($tid === T_STRING && !in_array($tcode, $this->allowed_functions, true)) { 
        $err = sprintf("Token or function '%s' not allowed", $tcode);
        return false;
      }
      
      if ($tid == T_VARIABLE)
        $varseen = $tcode;
      else
        $varseen = '';
      
      if ($tid == T_VARIABLE)
        $ret .= sprintf("\$user_variables['%s']", substr($tcode, 1));
      else
        $ret .= $tcode;
    }

    return $ret;

  }
  
  
  public function evaluate($user_variables) {
    $e = "return(" . $this->expr .");";
    return eval($e);
  }
}
