<?php

namespace app\components;

use yii\base\Exception;
use app\models\Flowchart;
use app\components\DialplanNode;
use app\models\Node;

class Dialplan {

	private $peerHangup = 0;
	private $varfile = null;
	
	protected $onhangup = -1;
	protected $onerror = -1;


	protected $nodes = array();
	protected $connections = array();
	
	const DS_NORMAL = 0;
	const DS_ERROR = 1;
	const DS_HANGUP = 2;

	const STATUS_MONITOR = "ST_MONITOR";
	const STATUS_DIALPLAN = "ST_DIALPLAN";

	const VAR_ERROR = "XIVO_IVR_ERROR";

	public function __construct($nodes, $conns) {
		foreach($nodes as $n) {
			$num = $n->node_number;
			$class = DialplanNode::typeToClassName($n['node_type']);
			$this->nodes[$num] = new $class($n->id);
			$this->connections[$num] = array();
		}

		foreach($conns as $c) {
			if (!isset($this->nodes[$c->src]))
				throw new Exception("Invalid dialplan structure");

			$this->connections[$c->src][$c->src_opt] = $c->dst;
		}

		$this->onerror = $this->onerrorNode();
		$this->onhangup = $this->onhangupNode();
	}

	public static function load($id) {
		$flowchart = Flowchart::find()->where(["id" => $id])->with("nodes.nodeprms", "nodecons")->one();
		
		if ($flowchart === null)
			return null;
		
		return new Dialplan($flowchart->nodes, $flowchart->nodecons);
	}


	public function hangup($agi) {
		$this->peerHangup = 1;
	}


	protected function onhangupNode() {
		foreach ($this->nodes as $idx => $n) {
			if ($n->getType() == Node::NODE_TYPE_ONERROR)
				return $idx;
		}
		
		return -1;
	}
	
	
	protected function onerrorNode() {
		foreach ($this->nodes as $idx => $n) {
			if ($n->getType() == Node::NODE_TYPE_ONHANGUP)
				return $idx;
		}
		
		return -1;
	}

	
	public function run($agi) {
		$dp_status = [self::STATUS_DIALPLAN => self::DS_NORMAL, self::STATUS_MONITOR => false];
		$user_variables = [];

		$agi->register_hangup_handler('dialplan', [$this, 'hangup']);
		
		$current = 0;
		$agi->cmd_set_variable("AGISIGHUP", "no");

		while (isset($this->nodes[$current])) {
			$n = $this->nodes[$current];
			$agi->debug("running node num=$current type=" . get_class($n)); 

			try {
				$opt = $n->run($agi, $dp_status, $user_variables);
			} catch (Exception $e) {
				$agi->cmd_set_variable(self::VAR_ERROR, "node_number: {$current} failed with exception " . $e->getMessage());
				$agi->debug("fail with exception");
				$opt = DialplanNode::OPT_ERROR;
			}

			if ($opt == DialplanNode::OPT_ERROR) {
				$agi->cmd_set_variable(self::VAR_ERROR, "node_number: {$current} failed");
			}
			$agi->debug("node {$current} returned: {$opt}");
			
			if ($opt == DialplanNode::OPT_ERROR && $dp_status[self::STATUS_DIALPLAN] != self::DS_ERROR) {
				$agi->debug("node error");
				if ($this->onerror > -1) {
					$agi->debug("starting onerror part of flowchart");
					$dp_status[self::STATUS_DIALPLAN] = self::DS_ERROR;
					$current = $this->onerror;
					continue;
				} else {
					return 1;
				}
			}					
			
			
			usleep(200 * 1000);
			$agi->test_hangup();

			if ($this->peerHangup && $dp_status[self::STATUS_DIALPLAN] == self::DS_NORMAL) {
				$agi->debug("hangup detected onhangup={$this->onhangup}");
				$agi->debug("peer hangup");
				
				if ($this->onhangup > -1) {
					
					$dp_status[self::STATUS_DIALPLAN] = self::DS_HANGUP;
					$current = $this->onhangup;
					continue;
				} else {
					return 0;
				}			
			} 
			
			if (!isset($this->connections[$current][$opt]))
				break;
			
			$current = $this->connections[$current][$opt];
			$agi->debug("Next node num=$current");

		}
		return 0;
	}
}

?>
