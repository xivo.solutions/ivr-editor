<?php

namespace app\components;

class onhangupNode extends DialplanNode {

  public static function optList() {
    return array('next');
  }

  public function run($agi, &$dp_status, &$user_variables) {
    return 'next';
  }
}

