<?php

namespace app\components;

use app\models\Groupfeatures;


class groupNode extends DialplanNode {

  public static function optList() {
    return [];
  }
  

  public function run($agi, &$dp_status, &$user_variables) {
    if (!($f = $this->evalParams($user_variables)))
      return self::OPT_ERROR;
    
    if (!$f->validate())
      return self::OPT_ERROR;

    $qf = Groupfeatures::find()->where(['name' => $f->grpname])->one();
    
    if (!$qf || !$qf->context || !$qf->number)
      return self::OPT_ERROR;
    
    $agi->exec_goto($qf->context, $qf->number, 1);
    return '';
  }
}

?>
