<?php

namespace app\components;

class hangupNode extends DialplanNode {

  public static function optList() {
    return array();
  }

  public function run($agi, &$dp_status, &$user_variables) {
    sleep(1);
    return '_hangup';
  }
}

?>
