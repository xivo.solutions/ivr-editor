<?php


namespace app\components;

use yii\validators\Validator;

class VariantValidator extends Validator {

  public $decision;
  public $rules1;
  public $rules2;


  public function validateAttribute($object, $attribute) {
    $vlist = array();

    if (!isset($this->decision) || preg_match($this->decision, $object->{$attribute}))
      $rules = $this->rules1;
    else
      $rules = $this->rules2;
      
    foreach ($rules as $r) {
        if (isset($r[0])) {
          $v = Validator::createValidator($r[0], $object, $attribute, array_slice($r, 1));
          $vlist[] = $v;
        }
    }
    
    foreach($vlist as $val)
      $val->validateAttribute($object, $attribute);
    
  }
} 
