<?php

namespace app\components;

use yii\filters\auth\AuthMethod;
use yii\helpers\ArrayHelper;
use yii\web\UnauthorizedHttpException;
use yii\base\InvalidConfigException;
use app\models\DummyUser;


class AuthXivo extends AuthMethod {

  public $url;
  public $cookie_name;
  public $timeout;
  public $connecttimeout;

	
  public function authenticate($user, $request, $response) {
    if (!$this->cookie_name)
      throw new InvalidConfigException;
    
    if (!$this->url)
      throw new InvalidConfigException;
    
    $cookie = ArrayHelper::getValue($_COOKIE, $this->cookie_name);
    if (!isset($cookie))
      return null;

    $timeout = $this->timeout ? $this->timeout : 3;
    $connecttimeout = $this->connecttimeout ? $this->connecttimeout : 3;

    $opts = [
      CURLOPT_TIMEOUT => $timeout,
      CURLOPT_CONNECTTIMEOUT => $connecttimeout,
      CURLOPT_FOLLOWLOCATION => false,
      CURLOPT_NOBODY => true,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_COOKIE => $this->cookie_name . "=" . $cookie,
      CURLOPT_SSL_VERIFYPEER =>  false,
      CURLOPT_SSL_VERIFYHOST =>  false
    ];
    
    
    $h = curl_init($this->url);
    curl_setopt_array($h, $opts);
    
    if (curl_exec($h) === false) {
      return null;
    }
    
    $code = curl_getinfo($h, CURLINFO_HTTP_CODE);
    curl_close($h);
    
        
    
    if ($code == 200) { 
      $identity = new DummyUser();
      if ($user->login($identity))
        return $identity;
    }

    return null;
  }


  public function challenge($response) {
  }
 
  public function handleFailure($response) {
    throw new UnauthorizedHttpException('Invalid credentials');
  } 
}
