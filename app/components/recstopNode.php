<?php

namespace app\components;

class recstopNode extends DialplanNode {

  public static function optList() {
    return array('next');
  }

  public function run($agi, &$dp_status, &$user_variables) {
    if (!isset($dp_status[Dialplan::STATUS_MONITOR]) || !$dp_status[Dialplan::STATUS_MONITOR])
      $agi->exec_stop_mixmonitor();
      
    $dp_status[Dialplan::STATUS_MONITOR] = 0;
    return 'next';
  }
}

