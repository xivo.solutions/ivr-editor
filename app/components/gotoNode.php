<?php

namespace app\components;

class gotoNode extends DialplanNode {

	public static function optList() {
		return [];
	}


	public function run($agi, &$dp_status, &$user_variables) {
	        if (!($f = $this->evalParams($user_variables)))
	                return self::OPT_ERROR;
    
                if (!$f->validate()) {
                        return self::OPT_ERROR;
                }
	
                $agi->exec_goto($f->context, $f->exten, 1);
                return '';
        }
}

?>
