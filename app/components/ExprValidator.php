<?php

namespace app\components;

use yii\validators\Validator;
use yii\base\Exception;
use app\components\Expression;

class ExprValidator extends Validator {

  public function validateAttribute($object, $attribute) {
    $result = $this->validateValue($object->{$attribute});

    if ($result === null)
      return;
    
    $this->addError($object, $attribute, $result[0]);
  }
  
  
  protected function validateValue($value) {
    if ($value[0] != '=') { 
      return ["Expression must start with '='", []];
    }
      
    try {
      $expr = new Expression(substr($value, 1));
    } catch (Exception $e) {
      return [$e->getMessage(), []];
    }
    
    return null;
  }
}
