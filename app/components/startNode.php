<?php

namespace app\components;

class startNode extends DialplanNode {

  public static function optList() {
    return array('next');
  }

  public function run($agi, &$dp_status, &$user_variables) {
    $agi->cmd_answer();
    return 'next';
  }
}

