<?php

namespace app\components;

use Yii;

class VoicePromptFile {

    private $path;
    private $name;
    private $type;

    const TYPE_VP = 'vp';
    const TYPE_REC = 'rec';
    const TYPE_MON = 'mon';

    
    private static $types = [
        self::TYPE_VP,
        self::TYPE_REC,
        self::TYPE_MON
    ];


    const ERR_OK = 0;
    const ERR_CONVERSION = -1;
    const ERR_STORE = -2;

    public function __construct($type, $name) {
        if (!($b = self::baseDir($type)))
            throw new Exception(Yii::t('app', "Invalid file type"));
            
        if (!self::validateName($name))
            throw new Exception(Yii::t('app', "Invalid file name"));
        
        $name = str_replace(" ", "_", $name);
        $this->type = $type;
        $this->name = $name;
        $this->path = $b . "/" . $name . ".wav";
    }


    private static function soundDir() {
        $dir = Yii::$app->params['clientSoundDir'];
        if (!$dir)
            throw new Exception(Yii::t('app', "Configuration error (clientSoundDir)"));
            
        return $dir;
    }


    private static function monDir() {
        $dir = Yii::$app->params['clientMonitorDir'];
        if (!$dir)
            throw new Exception(Yii::t('app', "Configuration error (clientMonitorDir)"));
            
        return $dir;
    }

    
    
    
    private static function baseDir($type) {
        if (!self::validateType($type))
            return null;
        
        if ($type === 'mon')
            return self::monDir() . "/";
        else
            return self::soundDir() . "/" . $type; 
        
    }

    public static function validateName($name) {
        return preg_match("/[[:alnum:]][[:alnum:]_-]*/", $name);
    }
    
    public static function validateType($type) {
        return in_array($type, self::$types, true);
    }
    
    public function delete() {
        if (!file_exists($this->path))
            return false;
            
        return unlink($this->path);
    }
        

    public static function listAll($type, $listbox = 0) {
        $ret = array();

        if (!($dir = self::baseDir($type)))
            return $ret;
        
//        echo "here $dir"; exit;
        
        if (!($dh = @opendir($dir)))
            return $ret;

        while (($fname = readdir($dh)) !== false) {
            if (!preg_match("/.wav\$/", $fname))
                continue;

            $name = substr($fname, 0, -4); 
            
            if ($listbox)
                $ret[$name] = $name;
            else
                $ret[] = array('name' => $name);
        }
        
        closedir($dh);
        
        asort($ret);
        return $ret;
    }


    public static function mkUnique($type) {
        $dir = self::baseDir($type);
        if (!$dir)
            return null;
            
        $dir .= "/";
        
        $prefix = strftime("rec-%Y%m%d-%H%M%S-");
        $fname = tempnam($dir, "tmp");
        
        echo ("fname: $fname \n");
        
        for ($i = 0; ; $i++) {
            $n = $prefix . $i;
            $fn = $dir . $n . ".wav";
            if (link($fname, $fn)) {
                unlink($fname);
                return new VoicePromptFile($type, $n);
            } else {
                if (!file_exist($fn))
                    return null;    
            }
        }
    }

/*
    public function mkFromText($text, $lang, $speed = 1) {
        $f = AudioFile::multi_tts($text, $lang);
        if ($f === false)
            return false;
        
        
        if ($speed != 1) {
            $f2 = AudioFile::changeTempo($f, $speed);
            unlink($f);
            
            if (!$f2)
                return false;

            $f = $f2;
        }
        
        exec("mv $f {$this->path}");
        return true;
    }
*/

    public function save($file, $keep = 0) {
        $tmp = AudioFile::convertToWav($file);

        if (!$tmp)
            return self::ERR_CONVERSION;;
            
        if (!$keep)
            unlink ($file);
        
        $dir = pathinfo($this->path, PATHINFO_DIRNAME);
        if (!is_dir($dir)) {
            exec("mkdir -p {$dir}", $out, $ret);
            if ($ret != 0)
                return self::ERR_STORE;;
        }
        
        $tmp = escapeshellarg($tmp);
        exec("mv $tmp {$this->path}", $out, $ret);
        
        return $ret == 0 ? self::ERR_OK : self::ERR_STORE;
    }

    
    public function rename($name) {
        if (!self::validateName($name))
            return 0;

        $name = str_replace(" ", "_", $name);
            
        $oldpath = $this->path;
        $newpath = dirname($oldpath) . "/" . $name . ".wav";

        if (!@link($oldpath, $newpath))
            return 0;
        
        $this->path = $newpath;
        $this->name = $name;
        
        unlink($oldpath);
        return 1;
    }


    public function exists() {
        return file_exists($this->path);
    }


    public static function getPlayPath($type, $name) {
        if (!self::validateName($name))
            return '';
            
        return self::baseDir($type) . "/" . $name;        
    }
    
    
    public function getPath() {
        return $this->path;
    }


    public static function errMsg($err) {
        switch ($err) {
        case self::ERR_OK:
            return Yii::t('app', "OK");
            break;
        case self::ERR_CONVERSION:
            return Yii::t('app', "File conversion failed");
            break;
        case self::ERR_STORE:
            return Yii::t('app', "Failed to store file");
            break;
        }
        
        return Yii::t('app', "Unknown error");
    }
}

?>
