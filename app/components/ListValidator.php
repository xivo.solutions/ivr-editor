<?php

namespace app\components;

use yii\validators\Validator;

class ListValidator extends Validator {

  public $min;
  public $max;
  public $rules;
  public $delimiter;

  public function validateAttribute($object, $attribute) {
    $vlist = array();
    $a = preg_split($this->delimiter, $object->{$attribute});
    
    if ($a == false)
      throw new Exception("Invalid delimiter");
    
    $this->min += 0;
    if (count($a) < $this->min) {
      $object->addError($attribute, "List too short, min {$this->min}");
      return;
    }
    
    if (isset($this->max)) {
      $max += 0;
      if (count($a) > $this->max) {
        $object->addError($attribute, "List too long, max {$this->max}");
        return;
      }
    }  
    
    foreach ($this->rules as $r) {
      if (isset($r[0])) {
        $v = Validator::createValidator($r[0], $object, $attribute, array_slice($r, 1));
        $vlist[] = $v;
      }
    }
    
    $origattr = $object->{$attribute};
    
    foreach($a as $item) {
      $object->{$attribute} = $item;
      foreach($vlist as $val)
        $val->validateAttribute($object, $attribute);
    }
  
    $object->{$attribute} = $origattr;
  
  }
} 
