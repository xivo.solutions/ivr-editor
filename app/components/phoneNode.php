<?php

namespace app\components;

class phoneNode extends DialplanNode {

	public static function optList() {
		return array('answer', 'busy', 'fail');
	}


	public function run($agi, &$dp_status, &$user_variables) {
	        if (!($f = $this->evalParams($user_variables)))
	                return self::OPT_ERROR;
    
                if (!$f->validate()) {
                        return self::OPT_ERROR;
                }
	
		$phone = Phone::model()->findByPk($f->id_phone);
                $opt = 'tk';
                if ($f->moh)
                        $opt .= 'm';
                $chan = "SIP/" . $phone->name;
                if ($f->number)
                        $chan .= "/" . $f->number;
                        
                
                		
		$ret = $agi->exec_dial($chan, $f->timeout, $opt);
		if ($ret != 0)
		        return self::OPT_ERROR;
		        
		$result = $agi->cmd_get_variable('DIALSTATUS');
		
		if ($result == 'ANSWER')
		        return 'answer';
                
                if ($result == 'BUSY' || $result == 'NOANSWER')
                        return 'busy';
                        
                return 'fail';
        }
}

?>
