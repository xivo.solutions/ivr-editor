<?php

namespace app\components;

use yii\validators\Validator;

class VoicePromptFileValidator extends Validator {

  public $allowEmpty = true;

  public function validateAttribute($object, $attribute) {
    if ($this->allowEmpty && $this->isEmpty($object->{$attribute}))
      return;

    if (!VoicePromptFile::validateName($object->{$attribute}))
      $object->addError($attribute, 'Invalid file name');
    
    return;
    
    
    // TODO is reasonable to check file existence ?
    
  }
}
