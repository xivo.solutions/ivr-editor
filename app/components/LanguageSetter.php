<?php


namespace app\components;


use yii\helpers\ArrayHelper;
use yii\base\ActionFilter;

class LanguageSetter extends ActionFilter {
    public $cookie_name;
    
    public function beforeAction($action) {
        if (is_string($this->cookie_name) && $this->cookie_name != '') {
            $lang = ArrayHelper::getValue($_COOKIE, $this->cookie_name);
            \Yii::$app->language = $lang;
        }
        
        return parent::beforeAction($action);
    }
}
         