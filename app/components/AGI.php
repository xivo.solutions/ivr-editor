<?php

namespace app\components;

class AGI {

  private $agivars;
  private $hangup_handlers = [];

  public function debug($str) {
    $this->cmd_verbose($str);
  }
  
  public function read_agi_variables() {
    $this->agivars = array();
    while (!feof(STDIN)) {
      $agivar = trim(fgets(STDIN));
      if ($agivar === '') {
      	break;
      } else {
      	$agivar = explode(':', $agivar);
        $this->agivars[$agivar[0]] = trim($agivar[1]);
      }
    }
  }


  private function quote_str($str) {
    return '"' . $str . '"';
  }


  private function call_hangup_handlers() {
    foreach($this->hangup_handlers as $name => $fnc) {
      call_user_func($fnc, $this);
    }
  }

  
  public function get_agi_var($var) {
    return isset($this->agivars[$var]) ? $this->agivars[$var] : null;
  }

  
  public function register_hangup_handler($name, $handler) {
    $this->hangup_handlers['name'] = $handler;
  }
  

  public function unregister_hangup_handler($name) {
    if (!isset($this->hangup_handlers['name']))
      return false;
      
    unset($this->hangup_handlers['name']);
    return true;
  }


  public function test_hangup() {
    stream_set_blocking(STDIN, false);
    $line = trim(fgets(STDIN));
    stream_set_blocking(STDIN, true);
    $this->debug("line: x{$line}x");

    if ($line == "HANGUP") {
      $this->call_hangup_handlers();
      return true;
    }
    
    return false;
  }
  
  
  public function cmd($cmd) {
    fwrite(STDOUT, $cmd . "\n");
    fflush(STDOUT);
    
    $result = trim(fgets(STDIN));
    
    if ($result == "HANGUP") {
      $this->call_hangup_handlers();
      $result = trim(fgets(STDIN));
    }
    
    $ret = array('code'=> -1, 'result'=> -1, 'timeout'=> false, 'data'=> '');

    if (preg_match("/^([0-9]{1,3}) (.*)/", $result, $matches)) {
      $ret['code'] = $matches[1];
      $ret['result'] = 0;
      if (preg_match('/^result=([0-9a-zA-Z]*)\s?(?:\(?(.*?)\)?|(.*))?$/', $matches[2], $match))  {
        $ret['result'] = $match[1];
        $ret['timeout'] = ($match[2] === 'timeout') ? true : false;
        $ret['data'] = $match[2];
      }
    }
    
    return $ret;
  }
  
  public function cmd_get_variable($var) {
    $cmd = sprintf ("GET VARIABLE %s",  $var);
    $ret = $this->cmd($cmd);
    if ($ret['result'] == 1)
    	return $ret['data'];
    else
      return "";
  }
  
  public function cmd_set_variable($var, $value) {
    $cmd = sprintf("SET VARIABLE %s \"%s\"", $var, $value);
    $ret = $this->cmd($cmd);
		
    return $ret['code'] == 200 ? $ret['result'] : -1;
  }
  
  public function cmd_get_option($file, $opts = "0123456789*#ABCD", $timeout = 10000) {
    $cmd = "GET OPTION $file $opts $timeout ";
    $ret = $this->cmd($cmd);

    return $ret['code'] == 200 ? $ret['result'] : -1;
  }
  
  public function cmd_stream_file($file, $opts = "0123456789*#ABCD") {
    $cmd = "STREAM FILE $file $opts";
    $ret = $this->cmd($cmd);

    return $ret['code'] == 200 ? $ret['result'] : -9;
  }
  
  public function cmd_answer() {
    $ret = $this->cmd("ANSWER");
    return $ret['code'] == 200 ? $ret['result'] : -9;
  }

  
  public function cmd_hangup($channel = '') {
    $cmd = sprintf("HANGUP %s", $channel);
    $ret = $this->cmd($cmd);
  
    return $ret['code'] == 200 ? $ret['result'] : -9;
  }
  
  public function cmd_exec($cmd) {
    $cmd = sprintf("EXEC %s", $cmd);
    $ret = $this->cmd($cmd);

    return $ret['code'] == 200 ? $ret['result'] : -9;
  }

  
  public function cmd_verbose($str, $level = 1) {
    $cmd = "VERBOSE \"$str\" $level";
    $ret = $this->cmd($cmd);
    return $ret['code'] == 200 ? $ret['result'] : -9;
  }

  public function cmd_say_number($number, $digits = '', $gender = 'm') {
    $cmd = "SAY NUMBER $number \"$digits\" $gender";
    $ret = $this->cmd($cmd);
    return $ret['code'] == 200 ? $ret['result'] : -9;
  }


// exec commands

  public function exec_playback($file) {
    return $this->cmd_exec(sprintf("Playback %s", $this->quote_str($file)));
  }


  public function exec_read($var, $max_digits, $options, $attempts = 1) {
    $cmd = "READ $var,$max_digits,$options,$attempts";
    return $this->cmd_exec($cmd);
  }
   
  public function exec_dial($chan, $timeout, $opt) {
    $cmd = "DIAL $chan,$timeout,$opt";
    return $this->cmd_exec($cmd);
  }


  public function exec_mixmonitor($file, $opts) {
    $cmd = "MixMonitor $file,$opts";
    return $this->cmd_exec($cmd);
  }
  
  public function exec_stop_mixmonitor() {
    $cmd = "StopMixMonitor";
    return $this->cmd_exec($cmd);
  }
  
  public function exec_wait($seconds) {
    $cmd = "Wait $seconds";
    return $this->cmd_exec($cmd);
  }  
  
  public function exec_celgenuserevent($name, $msg) {
    $cmd = "CelGenUserEvent {$name}," . $this->quote_str($msg);
    return $this->cmd_exec($cmd);
  }  

  public function exec_goto($context, $exten, $prio) {
    $cmd = "Goto $context,$exten,$prio";

    $prm = $prio;
    if ($exten) {
      $prm = $exten . "," . $prm;
      if ($context)
        $prm = $context . "," . $prm;
    }

    return $this->cmd_exec($cmd);
  }

}
