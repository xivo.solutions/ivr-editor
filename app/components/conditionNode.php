<?php

namespace app\components;

class conditionNode extends DialplanNode {

  public static function optList() {
    return array('1', '0');
  }
  
  public function run($agi, &$dp_status, &$user_variables) {
    if (!($f = $this->evalParams($user_variables)))
      return self::OPT_ERROR;
    
    if (!$f->validate())
      return self::OPT_ERROR;
    
    return $f->cond ? '1' : '0';
    
      
    return 'next';
  }
}

?>
