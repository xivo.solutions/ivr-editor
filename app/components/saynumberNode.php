<?php

namespace app\components;

class saynumberNode extends DialplanNode {

  public static function optList() {
    return array('next');
  }
  
  public function run($agi, &$dp_status, &$user_variables) {
    if (!($f = $this->evalParams($user_variables))) {
      return self::OPT_ERROR;
      
    }

    if (!$f->validate())
      return self::OPT_ERROR;
    
    $f->number += 0;
    $agi->cmd_say_number($f->number);
    return 'next';
  }
}

?>
