<?php

namespace app\components;

class Voicemail {

  const BASEDIR = "/var/spool/asterisk/voicemail";
    

  private $user;
  private $path;

  public function __Construct($user) {
    $this->user = $user;
    $this->path = self::BASEDIR . "/" . \Config::get("app.machine_name") . '/' . $user;
    $this->oldpath = $this->path . "/Old";
    $this->newpath = $this->path . "/INBOX";
  }
  
  
  private function getNewId($dir) {
    $id = 0;
    $flist = scandir($dir);
    foreach($flist as $f) {
      if (!preg_match('/^msg(\d\d\d\d)/', $f, $m))
        continue;
        
      if ($m[1] >= $id)
        $id = $m[1] + 1;
    }
    
    return sprintf("%04d", $id);
  } 
  
  
  public function mkMsgRead($id) {
    if (!$this->checkId($id) || $id[0] != 'n')
      return false;
    
    $audio_path_old = $this->getPathByID($id, 'a');
    $data_path_old = $this->getPathByID($id, 'd');
    
    if (!file_exists($audio_path_old))
      return false;

    $newid = $this->getNewId($this->oldpath);
    $newid = 'o' . $newid;

    $audio_path_new = $this->getPathByID($newid, 'a');
    $data_path_new = $this->getPathByID($newid, 'd');    

    rename($audio_path_old, $audio_path_new);
    rename($data_path_old, $data_path_new);
  
    return $newid;
  }
    
  
  
  
  private function parseMsgDataFile($f) {
    $ret = [];
    
    $lines = file($f, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    foreach($lines as $ln) {
      if (preg_match('/^callerid=(.*)$/', $ln, $m)) {
        $ret['callerid'] = $m[1];
        continue;
      }
      
      
      if (preg_match('/^origtime=(.*)$/', $ln, $m)) {
        $ret['origtime'] = $m[1]+0;
        continue;
      }
    }
    
    
    return $ret;
  }
  
  
  private function getDirById($id) {
    switch ($id[0]) {
      case 'n':
        return $this->newpath;
      case 'o':
        return $this->oldpath;
    }
    
    return false;    
  }


  
  private function checkId($id) {
    return preg_match('/^[on]\d\d\d\d$/', $id);
  }

  
  private function getLocalId($id) {
    if ($id[0] == 'n' || $id[0] == 'o')
      return substr($id, 1);
  }
  
  
  private function getMsgInfo($id) {
    $fullpath = $this->getPathById($id, 'd');

    if (file_exists($fullpath))
      $ret = $this->parseMsgDataFile($fullpath);
    else 
      $ret = [];
      
    $ret['id'] = $id;
      
    return $ret;  
  }
  
  
  private function getList($idprefix) {
    if (!($dir = $this->getDirById($idprefix)))
      return false;

    $ret = [];
    $flist = scandir($dir);
    foreach ($flist as $f) {
      if (!preg_match('/^msg(\d\d\d\d)\.wav$/', $f, $m))
        continue;
      
      $id = $idprefix . $m[1];
      $ret[] = $this->getMsgInfo($id);
    }

    return $ret;
  }      
      


  public function listNew() {
    return $this->getList('n');
  }
  
  
  public function listOld() {
    return $this->getList('o');
  }
  
  
  public function listAll() {
    $new = $this->listNew();
    $old = $this->listOld();

    if ($new === false || $old === false )
      return false;




    return array_merge($new, $old);
  }    
  

  public function delete($id) {
    if (!$this->checkId($id))
      return false;
    
    if ($f = $this->getPathById($id, 'a'))
      unlink($f);
    
    if ($f = $this->getPathById($id, 'd')) 
      unlink($f);
    
    return true;
  }
  
  
  private function getPathById($id, $type) {
    if ($type != 'a' && $type != 'd')
      return false;
    
    if (!($dir = $this->getDirById($id)))
      return false;
    
    $locid = $this->getLocalId($id);
    
    if ($type == 'a')
      return sprintf('%s/msg%04d.wav', $dir, $locid);
    else
      return sprintf('%s/msg%04d.txt', $dir, $locid);
  }
  
  public function getMsgPathById($id) {
    if (!$this->checkId($id))
      return false;
    
    return $this->getpathById($id, 'a');
  }
  
}  
  
  