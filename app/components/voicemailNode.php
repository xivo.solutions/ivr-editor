<?php

namespace app\components;

use app\models\Voicemail;

class voicemailNode extends DialplanNode {
  const DIALPLAN_CONTEXT = 'forward';
  const DIALPLAN_EXT = 'voicemail';

  const VAR_VOICEMAIL_ID = 'XIVO_VMBOXID';
  const VAR_TO_CLEAR = 'XIVO_FWD_ACTIONARG1';



  public static function optList() {
    return [];
  }


  public function run($agi, &$dp_status, &$user_variables) {
    if (!($f = $this->evalParams($user_variables)))
      return self::OPT_ERROR;

    if (!$f->validate())
      return self::OPT_ERROR;

    $vm = Voicemail::findOne($f->uniqueid);

    if (!$vm )
      return self::OPT_ERROR;

    $agi->cmd_set_variable(self::VAR_VOICEMAIL_ID, $vm->uniqueid);
    $agi->cmd_set_variable(self::VAR_TO_CLEAR, '');
    $agi->exec_goto(self::DIALPLAN_CONTEXT, self::DIALPLAN_EXT, 1);
    return '';
  }
}

?>
