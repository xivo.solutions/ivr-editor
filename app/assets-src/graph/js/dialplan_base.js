var jsPlumbInstance;

var csrfToken = $('meta[name="csrf-token"]').attr("content");	

	// this is the paint style for the connecting lines..
	var connectorPaintStyle = {
		lineWidth:4,
		strokeStyle:"black",
		joinstyle:"round",
		outlineColor:"white",
		outlineWidth:2
	},
	// .. and this is the hover style. 
	connectorHoverStyle = {
		lineWidth:4,
		strokeStyle:"rgb(255, 87, 34)",
		outlineWidth:2,
		outlineColor:"white"
	},
	endpointHoverStyle = {
		fillStyle:"rgb(255, 87, 34)",
		strokeStyle:"rgb(255, 87, 34)"
	},
	// the definition of source endpoints (the small ones on bottom)
	sourceEndpoint = {
		endpoint:"Dot",
		paintStyle:{ 
			strokeStyle:"rgb(255, 87, 34);",
			fillStyle:"rgb(255, 87, 34);",
			radius:4,
		},				
		isSource:true,
		connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
		connectorStyle:connectorPaintStyle,
		hoverPaintStyle:endpointHoverStyle,
		connectorHoverStyle:connectorHoverStyle,
		dragOptions:{},
	},		
	// the definition of target endpoints (big one on top) 
	targetEndpoint = {
		endpoint:"Dot",					
		paintStyle:{ fillStyle:"rgb(255, 87, 34);",radius:10 },
		hoverPaintStyle:endpointHoverStyle,
		maxConnections:-1,
		dropOptions:{ hoverClass:"hover", activeClass:"active" },
		isTarget:true,			
	};
	



jsPlumb.ready(function() {
	
	var instance = jsPlumb.getInstance({
		// default drag options
		DragOptions : { cursor: 'pointer', zIndex:2000 },
		// the overlays to decorate each connection with.  note that the label overlay uses a function to generate the label text; in this
		// case it returns the 'labelText' member that we set on each connection in the 'init' method below.
		ConnectionOverlays : [
			[ "Arrow", { location:1 } ],
			[ "Label", { 
				location:0.1,
				id:"label",
				cssClass:"aLabel"
			}]
		],
		Container:"flowchart-demo"
	});
	
	
	jsPlumbInstance = instance;
				
	init = function(connection) {			
		connection.getOverlay("label").setLabel(connection.sourceId.substring(15) + "-" + connection.targetId.substring(15));
		connection.bind("editCompleted", function(o) {
			if (typeof console != "undefined")
				console.log("connection edited. path is now ", o.path);
		});
	};			
	


	// suspend drawing and initialise.
	instance.doWhileSuspended(function() {

					
		// listen for new connections; initialise them the same way we initialise the connections at startup.
		instance.bind("connection", function(connInfo, originalEvent) { 
			init(connInfo.connection);
		});			
					
		
//		drawAllNodes(NodePos);
		
		//
		// listen for clicks on connections, and offer to delete connections on click.
		//
		instance.bind("click", function(conn, originalEvent) {
			if (confirm("Delete connection from " + conn.sourceId + " to " + conn.targetId + "?"))
				jsPlumb.detach(conn); 
		});	
		
		instance.bind("connectionDrag", function(connection) {
			console.log("connection " + connection.id + " is being dragged. suspendedElement is ", connection.suspendedElement, " of type ", connection.suspendedElementType);
		});		
		
		instance.bind("connectionDragStop", function(connection) {
			console.log("connection " + connection.id + " was dragged");
		});

		instance.bind("connectionMoved", function(params) {
			console.log("connection " + params.connection.id + " was moved");
			move_connection(params);
		});

		instance.bind("connectionDetached", function(params) {
			console.log("connection " + params.connection.id + " detached");
			delete_connection(params.sourceId, params.sourceEndpoint.getUuid());
		});

		instance.bind("connection", function(params, e) {
			console.log("connection " + params.connection.id + " created");
			console.log("srcId: " + params.sourceId + "targetId: " + params.targetId);
			console.log("srcEndpoint: " + params.sourceEndpoint.getUuid() + "targetEndpoint: " + params.targetEndpoint.getUuid());
			create_connection(params);			
		});




	});

	jsPlumb.fire("jsPlumbDemoLoaded", instance);
	
});
