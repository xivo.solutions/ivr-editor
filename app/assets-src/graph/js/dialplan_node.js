
var NodeNum = 0;

var SuppressCreateCon = 0;
var SuppressDelCon = 0;

var LastCon = 0;

var Nodes = [];

var csrfToken = $('meta[name="csrf-token"]').attr("content");	

var NodePos = [
  {left: 350, top: 30}
];



function conNum(conid) {
  var n = conid.slice(4);
  var id = parseInt(n);
  
  return id;
}
  

function uuid2opt(uuid) {
  var idx = uuid.indexOf('-');

  return uuid.slice(idx+1);
}



function getNodeById(id) {
  ret = null;
  
  for (var i = 0; i < Nodes.length; i++) {
    if (Nodes[i].id == id) {
      ret = Nodes[i];
      break;
    }
  }
  
  return ret;
}



function getNodeByNum(num) {
  ret = null;
  
  for (var i = 0; i < Nodes.length; i++) {
    if (Nodes[i].node_number == num) {
      ret = Nodes[i];
      break;
    }
  }
  
  return ret;
}




  
function drawCon(nt) {
  var html = '';
  if (nt.con.length == 0)
    return html;
  html += '<table class="table-node-con">';
  html += '<tr>';
  for (var i = 0; i < nt.con.length; i++) {
    html += '<td class="td-node-con">' + nt.con[i] + '</td>';
  }
  html += '</tr>';
  html += '</table>';
  return html;
}


function ondblclickNodeName(e) {

  $("#div-node-name-form").remove(); 
  $("#div-node-prm-form").remove();
  

  var id = $(e).parent().attr('nodeid');
  
  var node = getNodeById(id);
  
  var pos = getNodeFormPos($(e));
  
  showNodeForm(node, pos, yii.urls.node_update, "div-node-name-form");
}



function onclickNodePrm(el) {
  $("#div-node-name-form").remove(); 
  $("#div-node-prm-form").remove();
  
  
  var elnode = $(el).parent();
  var id = elnode.attr('nodeid');
  
  var node = getNodeById(id);
  
  var pos = getNodeFormPos(elnode);
  
  showNodeForm(node, pos, yii.urls.node_parameters, "div-node-prm-form");
}
  

function onclickDeleteNode(img) {
  var nbox = $(img).parent();
  var id = nbox.attr('nodeid');

  var node = getNodeById(id);
  $.post(yii.urls.node_delete + "?id=" + node.id).done(function(ret) {
    if (ret) {
      var ep = jsPlumbInstance.getEndpoints(nbox);  
  
      SuppressDelCon = 1;
      for (var i = 0; i < ep.length; i++)
        jsPlumbInstance.deleteEndpoint(ep[i]);   
      SuppressDelCon = 0;

      deleteNodeById(id);
      nbox.remove();
    } else
      alert('Node deltion failed');
  }).fail(function() {
    alert('Node deltion failed');
  });
      
}
  
  
function deleteNodeById(id) {
  for (var i = 0; i < Nodes.length; i++) {
    if (Nodes[i].id == id) {
      Nodes.splice(i, 1);
      return;
    }
  }
}



function drawNode(node, pos) {
  var html = '';
  var nt = NodeType[node.node_type];  
  var cnt = nt.con.length; 
  var posstr = 'style="left: ' + pos.x + 'px; top: ' + pos.y + 'px"';
  
  
  html += '<div class="node" id="'+node.node_number+'" nodeid="'+node.id + '" ' + posstr + '">';
  if (node.node_number != 0)
    html += '<i class="fa fa-close del-btn" onclick="onclickDeleteNode(this)"></i>';
    
  html += '<i class="fa fa-edit edit-btn" onclick="onclickNodePrm(this)"></i>';
  
  
  html += '<div class="node-type">' + node.node_type + '</div>';  
  html += '<div class="node-name" ondblclick="ondblclickNodeName(this)">' + node.name + '</div>';

  html += drawCon(nt);
  html += '</div>';
  
  $('#flowchart').append(html);
  activateNode(node);
}

function countAnchor($e, $c) {
  var pos = $e.offset();
  var w = $e.width();
  var cpos = $c.offset();
  var cw = $c.outerWidth();
//  var ch = c.height();
  
  var x = (cpos.left - pos.left + cw / 2);
//  var y = (cpos.top - pos.top + ch / 2);
  
  return [0, 1, 0, 1, x, 0 ];

}

function activateSourceAnchors($e) {
  var num = $e.attr('id');
  
  var cells = $e.find('.td-node-con');
  
  cells.each(function(idx, cell) {
    var $cell = $(cell);
    var a = countAnchor($e, $cell);
    jsPlumbInstance.addEndpoint($e, sourceEndpoint, {anchor: a, uuid: num + "-" + $cell.html()});
  });
}


function updateSourceAnchors($e) {
  var cells = $e.find('.td-node-con');
  
  cells.each(function(idx, cell) {
    var $cell = $(cell);
    var uuid = $e.attr('id') + '-' + $cell.html();
    var ep = jsPlumbInstance.getEndpoint(uuid);
    var a = countAnchor($e, $cell);
    ep.setAnchor(a, true);
  });
  jsPlumbInstance.repaint($e[0]);
}
    
  
function activateTargetAnchor($e) {
  var num = $e.attr('id');
  
  jsPlumbInstance.addEndpoint($e, targetEndpoint, {anchor: "TopCenter", uuid: num});
} 
  

function onNodeDragStop(event, ui) {
  var id = ui.helper.attr('nodeid');
  $.post(yii.urls.nodepos_update + "?id=" + id, {Nodepos: {x: ui.position.left | 0, y: ui.position.top | 0}});
}



function activateNode(node) {
  e = $('#' + node.node_number);
  activateSourceAnchors(e);
  
  if (node.node_type != 'start' && node.node_type != 'onerror' && node.node_type != 'onhangup')
    activateTargetAnchor(e);
  
  jsPlumbInstance.draggable(e, {grid: [10, 10], stop: onNodeDragStop});      
}  
  


function onclickMenuItem(item) {
  var type = $(item).attr('type');
  
  mkNode(type, {});
}


function mkNode(type) {
  var node = {};
  var pos = {x: 10, y: 100};
  
  node.node_number = NodeNum++;
  node.node_type = type;
  node.id_flowchart = DialplanID;
  node.name = node.type;
  
  $.post(yii.urls.node_create, {"Node": node, "Nodepos": pos}).done(function(ret) {
    if (ret) {
      node.id = ret.id;
      node.name = ret.name;
      Nodes.push(node);
      drawNode(node, pos);
    } else
      alert('Node creation failed');
  }).fail(function() {
    alert('Node creation failed');
  });
}
  

function drawAllNodes(pos_array) {
  for (var i = 0; i < Nodes.length; i++) {
    drawNode(Nodes[i], pos_array[i]);
  }
}

function getNodeFormPos(node) {
  var pos = node.offset();
  pos.top += node.outerHeight();
  
  return pos;
}




function closeForm(id) {
  var form = $('#' + id);
  form.remove();
  
}


function updateNodeName(id, name) {
    node = getNodeById(id);
    node.name = name;
    var $el = $('#' + node.node_number);
    
    $el.find('.node-name').html(name);
    updateSourceAnchors($el); 
}

function showNodeForm(node, pos, url, eid) {
  html = '';

  // make position integer
  pos.left = ~~pos.left;
  pos.top = ~~pos.top;
  
  html += '<div id="' + eid + '" nodeid="' + node.id + '" ';
  html += 'style="left: '+pos.left + 'px; top: ' + pos.top + 'px; display: none">';
  html += '</div>';
  
  $('body').append(html);

  $.get(url + "?id=" + node.id).done(function(ret) {
    if (ret.success) { 
      var div = $('#' + eid);
      div.html(ret.html);
      div.css({display: 'initial'});
      div.find('.switch-off').prop('disabled', true);
    } else {
      closeForm(eid);
      alert("done but fail");
    }
  }).fail(function() {
    closeForm(eid);
    alert("fail");
  });
}



function submitNodeNameForm() {
  var form = $("#form-node-name");
  var id = form.parent().parent().attr("nodeid");
  var data = form.serialize();

  $.post(yii.urls.node_update + "?id=" + id, data).done(function(ret) {
    if (ret.success) {
      if (ret.name)
        updateNodeName(id, ret.name);
      closeForm("div-node-name-form");
    } else {
      form.html(ret.html);
    }
  }).fail(function() {
    $("#div-node-name .div-error").html("Save failed");
  });
  
  return false;
}


function submitNodePrmForm() {
  var form = $("#form-node-prm");
  var id = form.parent().parent().attr("nodeid");
  var data = form.serialize();

  $.post(yii.urls.node_parameters + "?id=" + id, data).done(function(ret) {
    if (ret.success) {
      if (ret.name) {
        updateNodeName(id, ret.name);
      }
      closeForm("div-node-prm-form");
    } else {
      var form = $("#div-node-prm-form");
      form.html(ret.html);
      form.find(".switch-off").prop("disabled", true);
      alert("Parametres update failed");
    }
  }).fail(function() {
    alert("Save failed");
  });
  
  return false;
}


function create_connection(params) {
  var id = conNum(params.connection.id);
  
  if (id <= LastCon)
    return;
  else
    LastCon = id;
  
  if (SuppressCreateCon)
    return;

  var opt = uuid2opt(params.sourceEndpoint.getUuid());

  var con = {
    id_flowchart: DialplanID,
    src: params.sourceId,
    src_opt: opt,
    dst: params.targetId
  };
  
  
  $.post(yii.urls.nodecon_create, {Nodecon: con}).done(function(ret) {
    if (ret != 1)
      alert("Connection creation failed");
  }).fail(function() {
    alert("Connection creation failed");
  });
}
  

function delete_connection(srcid, epid) {

  if (SuppressDelCon)
    return;

  var opt = epid.slice(epid.indexOf("-")+1);

  var con = {
    id_flowchart: DialplanID,
    src: srcid,
    src_opt: opt
  };
  
  
  $.post(yii.urls.nodecon_delete, {Nodecon: con}).done(function(ret) {
    if (ret != 1)
      alert("Connection deletion failed");
    }).fail(function() {
      alert("Connection deletion failed");
    });
}



function move_connection(params) {
  var oldcon = {
    id_flowchart: DialplanID,
    src: params.originalSourceId,
    src_opt: uuid2opt(params.originalSourceEndpoint.getUuid())
  };
  
  var newcon = {
    id_flowchart: DialplanID,
    src: params.newSourceId,
    src_opt: uuid2opt(params.newSourceEndpoint.getUuid()),
    dst: params.newTargetId
  };
  
  $.post(yii.urls.nodecon_move, {New: newcon, Old: oldcon}).done(function(ret) {
    if (ret != 1)
      alert("Connection move failed");
  }).fail(function() {
    alert("Connection move failed");
  });
}
  


function load() {
  if (DialplanID <= 0)
    return;
    
  $.getJSON(yii.urls.load + "?id=" + DialplanID).done(function(ret) {
    if (ret == '')
      alert("Load failed");
    else
      procLoadData(ret);
  }).fail(function() {
    alert("Load failed");
  }).always(function() {
  });
}


function procLoadData(data) {
  clearAll();
  
  var con = data.nodecons;
  var pos = [];

  Nodes = data.nodes;
    
  
  
  if (Nodes.length == 0) {
    alert("Load failed");
    return;
  }
    
  var num = 0; 
  for (var i = 0; i < Nodes.length; i++) {
    var n = parseInt(Nodes[i].node_number);
    pos[i] = Nodes[i].nodepos;
    if (num < n)
      num = n;
  }
  
  NodeNum = num  + 1;

  $("#Flowchart_name").val(data.name);

  drawAllNodes(pos);

  SuppressCreateCon = 1;
  drawAllConnections(con);
  SuppressCreateCon = 0;
}



function drawAllConnections(conns) {
  for (var i = 0; i < conns.length; i++) {
    var ep = [];
    ep[0] = conns[i].src + "-" + conns[i].src_opt;
    ep[1] = conns[i].dst;
    var c = jsPlumbInstance.connect({uuids: ep, editable: true});
  }
}
     


function clearAll() {
  SuppressDelCon = 1;
  jsPlumbInstance.detachEveryConnection();
  jsPlumbInstance.deleteEveryEndpoint();
  $('#flowchart .node').remove();
  SuppressDelCon = 0;
  
}


function dialplan_init() {
  if (DialplanID  > 0)
    load();
  else
    alert("Load failed");
}



function switch_input(el) {
  var cls = $(el).parent().attr('swclass');
  
  var on = $(".switch-on." + cls);
  var off = $(".switch-off." + cls);
  
  on.removeClass("switch-on");
  on.addClass("switch-off");
  on.prop('disabled', true);
  
  off.removeClass("switch-off");
  off.addClass("switch-on");
  off.prop('disabled', false);
  
}

