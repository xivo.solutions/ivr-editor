$(document).ready(function() {
    var html = '<audio class="call-player" src="" controls style="position: absolute"></audio>';

    $("body").append(html);
    $(".call-player").hide();

    $('.call-player').on('focusout', function() {
        $('.call-player').hide();
    });
});


function onclickPlay(e) {
        e = e || window.event;
        e.preventDefault();


        var p = $('.call-player'); 
        
        var $el = $(e.currentTarget);
        
        if (!$el.is("a"))
            $el = $el.parents("a");
        
        if (!$el)
            return;
        
        
        var href = $el.attr('href');

        p.attr('src', href);

        p.css('top', (e.pageY + 'px'));
        p.css('left', (e.pageX - p.width()) + 'px');
        p.show();
        p.focus();
        p[0].play()
}
