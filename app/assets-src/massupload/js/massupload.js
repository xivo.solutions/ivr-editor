var Files = {};
var Index = 0; 

$('#button-upload').on('click', submit);
$('#button-select').on('click', function(event) {
    $('#fileinput').click();
});

$('#fileinput').on('change', onchange_file);

function onchange_file() {
    newfiles = $('#fileinput')[0].files

    newdict = {};
    for(i=0; i < newfiles.length; i++) {
        var ord = "ord_" + Index;
        Index += 1;
        Files[ord] = newfiles[i];
        newdict[ord] = newfiles[i];
    }
    
    add_files(newdict);
}


function del_file(event) {
    ord = event.data;
    delete Files[ord];
    del_filerow(ord);
}

function del_filerow(ord) {
    row = $('#' + ord);
    row.remove();

    if (Object.keys(Files).length == 0) {
        $('.empty').parents('tr').show();
    }
}


function gen_name_input_cell(name) {
    var n = name.replace('"', '_');
    n = n.replace(/\.[^/.]+$/, "");
    return '<td><input name="name[]" value="' + n + '"></td>';
}
    

function gen_del_file_cell() {
    return '<td><span class="del-button glyphicon glyphicon-trash"></span></td>';
}


function add_files(newfiles) {
    if (newfiles.length == 0)
        return;
        
    $('.empty').parents('tr').hide();
    
    for (var ord in newfiles) {
        var html = '';
        
        html += '<tr id="' + ord + '">' ;
        html += '<td>' + newfiles[ord].name + '</td>';
        html += gen_name_input_cell(newfiles[ord].name);
        html += gen_del_file_cell(ord);
        html += '<td class="result"></td>'
        html += '</tr>';
        $('#voiceprompt-massupload-grid tbody').append(html);
        $('#' + ord + ' .del-button').on('click', null, ord, del_file);
    }
}

function result(ord, html) {
    $('#' + ord + ' .result').html(html);
}

function mark_ok(ord) {
    html = '<span style="color: green" class="glyphicon glyphicon-ok"></span>';
    result(ord, html);
}

function mark_err(ord) {
    html = '<span style="color: red" class="glyphicon glyphicon-warning-sign"></span>';
    result(ord, html);
}


function post_success(ret, ord) {
    if (ret === true) {
        mark_ok(ord);
        delete Files[ord];
        setTimeout(function() {
            del_filerow(ord);
        }, 1500);
        
        if (Object.keys(Files).length == 0) { 
            setTimeout(function() {
                window.location.href = indexUrl;
            }, 1500);
        }
    } else {
        mark_err(ord);
        show_error(ret);
    }
}


function post_failed(ord, status) {
    mark_err(ord);
    show_error(status);
}

function show_error(errstr) {
    $('#errors ul').append('<p><li>' + errstr + '</li></p>');
}


function clean_errors() {
    $('#errors ul').html('');
    $('.result').html('');
}


function submit() {
    clean_errors();

    for (var ord in Files) {
        const form = new FormData();
        form.append("_csrf", $('#voiceprompt-massupload-form input[name="_csrf"]').val())
        form.append('Voiceprompt[name]', $('#' + ord + ' input').val());
        form.append('Voiceprompt[file]', Files[ord]);
        form.append('Voiceprompt[message]', '');

        $.ajax({
            type: "POST",
            url: submitUrl,
            data: form,
            processData: false,
            contentType: false,
            ord: ord,
            success: function(ret) {
                post_success(ret, this.ord);
            },
            error: function(xhr, status) {
                post_failed(ord, status)
            }
        });
    }
    
    return true;
}
