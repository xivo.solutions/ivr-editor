<?php

namespace tests\unit\models;

use app\models\Queuefeatures;

class QueuefeaturesTest extends \Codeception\Test\Unit
{
    public function testTableName() {
        $this->assertEquals('queuefeatures', Queuefeatures::tableName());
    }

    public function testRules() {
        $this->assertEquals(Array(), Queuefeatures::rules());
    }

    public function testAttributeLabels() {
        $this->assertEquals(Array('name' => 'Name'), Queuefeatures::attributeLabels());
    }

}
