<?php

namespace tests\unit\models;

use app\components\DialplanNode;
use yii\base\Exception;

class DialplanNodeTest extends \Codeception\Test\Unit
{
    public function testTypeToClassName() {
        $this->assertEquals('app\components\DialplanNode', DialplanNode::typeToClassName("Dialplan"));

        try {
            DialplanNode::typeToClassName("SomeNode");
        } catch (Exception $e) {
            $this->assertInstanceOf(Exception::class, $e);
        }
        
    }

}
